﻿using House_of_Taste.Telas.Menu;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace House_of_Taste.Telas.TeladeSplash
{
    public partial class TelaSplash : Form
    {
        public TelaSplash()
        {
            InitializeComponent();
            //Começa a contagem para o início do SPLASH
            Task.Factory.StartNew(() =>
            {
                //Espera cerca de 5 segundos.
                System.Threading.Thread.Sleep(10000);

                Invoke(new Action(() =>
                {
                    Logar logar = new Logar();
                    logar.Show();
                    this.Hide();

                }));
            });
        }
    }
}
