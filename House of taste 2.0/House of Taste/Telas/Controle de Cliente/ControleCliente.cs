﻿using House_of_Taste.BancodeDados.ClassesBDPedido;
using House_of_Taste.ClassesBDCardapio;
using House_of_Taste.ClassesBDCliente;
using House_of_Taste.ClassesBDFuncionario;
using House_of_Taste.Telas.Menu;
using House_of_Taste.Utilitarios;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace House_of_Taste.Telas.Controle_de_Cliente
{
    public partial class ControleCliente : Form
    {
        int pk;
        decimal total = 0;
        int idfuncionario = UserSession.UsuarioLogado.Id_funcionario;
        BindingList<GriedViewPedido> listadepedido = new BindingList<GriedViewPedido>();
        int idcliente;
        string nomecliente, cpfcliente, telefonecliente;
        public ControleCliente()
        {
            InitializeComponent();
            CarregarCombo();
            ConfigurarGrid();
        }

        private void textBox13_TextChanged(object sender, EventArgs e)
        {
            //pode-se colocar um checkbox ara o pedido, exemplo:
            //qual tipo de refrigerante?.... etc
        }

        public void CarregarCombo()
        {
            CardapioBusiness business = new CardapioBusiness();
            List<CardapioDTO> produto = business.Listar();
            cboProdutos.DisplayMember = nameof(CardapioDTO.Nm_produto);
            cboProdutos.ValueMember = nameof(CardapioDTO.Id_cardapio);
            cboProdutos.ValueMember = nameof(CardapioDTO.Vl_valor);

            cboProdutos.DataSource = produto;
        }

        public void ConfigurarGrid()
        {
            dtg_consultarproduto.AutoGenerateColumns = false;
            dtg_consultarproduto.DataSource = listadepedido;
        }

        private void textBox8_TextChanged(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void label1_Click(object sender, EventArgs e)
        {
            TelaInicial menu = new TelaInicial();
            menu.Show();
            this.Close();
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            Menu.TelaInicial menu = new Menu.TelaInicial();
            menu.Show();
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            ConsultarPedido consultarpedido = new ConsultarPedido();
            consultarpedido.Show();
            this.Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            ConsultarClientes consultarclientes = new ConsultarClientes();
            consultarclientes.Show();
            this.Close();
        }

        private void DadosCliente()
        {
            if (idcliente != 0)
            {
                pk = idcliente;
            }
            else
            {
                if (txtnomecliente.Text != null || mktcpf.Text != null || mktcontato.Text != null || formapagamento.SelectedItem != null || txtnomecliente.Text != null)
                {
                    ClienteDTO cliente = new ClienteDTO();
                    cliente.Celular = mktcontato.Text;
                    cliente.CPF = mktcpf.Text;
                    cliente.NomeCompleto = txtnomecliente.Text;
                    cliente.FKIDFuncionario = idfuncionario;
                    ClienteBusiness salvacliente = new ClienteBusiness();
                    pk = salvacliente.Salvar(cliente);
                }
                else if (txtnomecliente.Text == null || mktcpf.Text == null || mktcontato.Text == null || formapagamento.SelectedItem != null || txtnomecliente.Text != null)
                {
                    MessageBox.Show("Por favor preencha todos os campos", "House of Taste", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }

        }

        public void DadosPedido()
        {
            for (int i = 0; i < dtg_consultarproduto.Rows.Count; ++i)
            {
                total = total + Convert.ToDecimal(dtg_consultarproduto.Rows[i].Cells[1].Value);
            }

            DTO_Pedidos dados = new DTO_Pedidos();
            dados.ID_Cliente = pk;
            dados.DataVenda = DateTime.Now;

            dados.ID_Funcionario = idfuncionario;
            dados.FormadePagamento = formapagamento.SelectedItem.ToString();
            GriedViewPedido cadastroProduto = listadepedido[0];

            dados.ValorPago = total;
            Business_Pedidos business = new Business_Pedidos();
            business.Salvar(dados, listadepedido.ToList());


        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (mktcontato.Text != "(  )     -")
            {
                if (mktcontato.Text.Length != 14 || mktcontato.Text.Length != 15)
                    MessageBox.Show("Digite o telefone corretamente");

                if (mktcontato.Text.Substring(0, 4).Contains(" "))
                    MessageBox.Show("Digite o telefone corretamente");

                if (mktcontato.Text.Substring(5).Contains(" "))
                    MessageBox.Show("Digite o telefone corretamente");

                Regex regra1 = new Regex(@"^\([0-9]{2}\)$");
                Regex regra2 = new Regex(@"^\([0-9]{2}\)[0-9]{4}-[0-9]{4}$");

                if (regra1.IsMatch(mktcontato.Text.Substring(1, 2)) != false)
                    MessageBox.Show("DD é inválido!");

                if (regra2.IsMatch(mktcontato.Text) != false)
                    MessageBox.Show("O telefone é invalido!");
            }


            if (txtnomecliente.Text == string.Empty)
                MessageBox.Show("O nome não pode estar em branco.");
            try
            {
                DadosCliente();
                DadosPedido();
                MessageBox.Show("Dados salvos com sucesso.", "House of Taste", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
           
        }

        private void cboProdutos_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void mktcpf_TextChanged(object sender, EventArgs e)
        {
            string cpf = mktcpf.Text;

            Regex rg = new Regex(@"^\d{3}\.\d{3}\.\d{3}-\d{2}$");

            bool validou = rg.IsMatch(mktcpf.Text);
            if (validou == true)
            {
                mktcpf.BackColor = Color.LawnGreen;
            }
            else
            {
                mktcpf.BackColor = Color.IndianRed;
            }
        }

        private void ControleCliente_Load(object sender, EventArgs e)
        {

        }

        private void CaregarPedido()
        {
            decimal qtde = nupquantidade.Value;
            if (qtde == 0)
            {
                MessageBox.Show("Por favor preencha o campo quantidade", "House of Taste", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {

                if (qtde != 0)
                {
                    CardapioDTO dto = cboProdutos.SelectedItem as CardapioDTO;
                    CardapioBusiness business = new CardapioBusiness();
                    List<CardapioDTO> lista = business.Listar();



                    for (int i = 0; i < qtde; i++)
                    {
                        GriedViewPedido itens = new GriedViewPedido();

                        itens.idproduto = dto.Id_cardapio;
                        itens.produto = dto.Nm_produto;
                        itens.quantidade = nupquantidade.Value;
                        itens.observacao = txtobs.Text;
                        itens.preco = dto.Vl_valor;
                        listadepedido.Add(itens);
                    }

                }

                else if (qtde == 0)
                {
                    MessageBox.Show("Por favor preencha o campo quantidade", "House of Taste", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            CaregarPedido();
        }
    }
}
