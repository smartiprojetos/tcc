﻿using House_of_Taste.ClassesBDFuncionario;
using House_of_Taste.ClassesBDProduto;
using House_of_Taste.Telas.Menu;
using House_of_Taste.Telas.Produtos;
using House_of_Taste.Utilitarios;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace House_of_Taste.Telas
{
    public partial class CadastroProduto : Form
    {
        public CadastroProduto()
        {
            InitializeComponent();
        }

        private void SalvarDados()
        {
            int fk = UserSession.UsuarioLogado.Id_funcionario;
            ProdutoDTO dto = new ProdutoDTO();
            dto.nome_produto = txtProduto.Text;
            dto.preco = decimal.Parse(txtPreco.Text);
            dto.validade = dtpValidade.Value;
            dto.fk_funcinario = fk;
            ProdutoBusiness salva = new ProdutoBusiness();
            salva.Salvar(dto);
        }

        private void LimparCampos()
        {
            txtProduto.Text = string.Empty;
            txtPreco.Text = string.Empty;
            dtpValidade.Value = DateTime.Now;
        }


        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
           
        }

        private void label1_Click(object sender, EventArgs e)
        {
            TelaInicial menu = new TelaInicial();
            menu.Show();
            this.Close();
        }

        private void CadastroProduto_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                exReg validar = new exReg();
                validar.ValidarValor(txtPreco.Text);
                validar.ValidarNome(txtProduto.Text);
                
                SalvarDados();
                MessageBox.Show("Dados salvos com sucesso.", "Salvo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

            catch (Exception ex)
            {
                MessageBox.Show("Por favor insira todos os dados corretamente.", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            Menu.TelaInicial menu = new Menu.TelaInicial();
            menu.Show();
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            ConsultarProdutos consultarprodutos = new ConsultarProdutos();
            consultarprodutos.Show();
            this.Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            LimparCampos();
            MessageBox.Show("Campos cancelados.");
        }
    }
}
