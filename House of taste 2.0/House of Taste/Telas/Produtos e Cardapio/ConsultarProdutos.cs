﻿using House_of_Taste.ClassesBDEstoque;
using House_of_Taste.ClassesBDProduto;
using House_of_Taste.Telas.Menu;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace House_of_Taste.Telas.Produtos
{
    public partial class ConsultarProdutos : Form
    {
        public ConsultarProdutos()
        {
            InitializeComponent();
        }

        private void dtg_consultarproduto_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            //não necessário

            //if (e.ColumnIndex == 3)
            //{
            //    ProdutoDTO dto = dtg_consultarproduto.CurrentRow.DataBoundItem as ProdutoDTO;
            //    ProdutoBusiness alterarproduto = new ProdutoBusiness();
            //    alterarproduto.Alterar(dto);
            //}

            //if (e.ColumnIndex == 4)
            //{
            //    ProdutoDTO dto = dtg_consultarproduto.CurrentRow.DataBoundItem as ProdutoDTO;
            //    int pk = dto.Id_cadastro_produto;
            //    ProdutoBusiness business = new ProdutoBusiness();
            //    business.Remover(pk);

            //}
        }

        private void label5_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void label1_Click(object sender, EventArgs e)
        {
            TelaInicial telainicial = new TelaInicial();
            telainicial.Show();
            this.Close();
          
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            Menu.TelaInicial menu = new Menu.TelaInicial();
            menu.Show();
            this.Close();

        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            CadastroProduto cadastroproduto = new CadastroProduto();
            cadastroproduto.Show();
            this.Close();
        }

        private void CarregarDados()
        {
            ProdutoBusiness business = new ProdutoBusiness();
            dtg_consultarproduto.AutoGenerateColumns = false;
            dtg_consultarproduto.DataSource = business.Consultar(txtproduto.Text);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            CarregarDados();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            ProdutoBusiness produtobusiness = new ProdutoBusiness();
            ProdutoDTO dto = dtg_consultarproduto.CurrentRow.DataBoundItem as ProdutoDTO;
            produtobusiness.Remover(dto.Id_cadastro_produto);
            CarregarDados();
            MessageBox.Show("Os dados selecionados foram deletados.");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            ProdutoBusiness produtobusiness = new ProdutoBusiness();
            ProdutoDTO dto = dtg_consultarproduto.CurrentRow.DataBoundItem as ProdutoDTO;
            produtobusiness.Alterar(dto);
            CarregarDados();
            MessageBox.Show("Os dados selecionados foram alterados.");
        }

        private void txtproduto_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void txtproduto_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
