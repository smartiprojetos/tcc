﻿using House_of_Taste.ClassesBDCardapio;
using House_of_Taste.Telas.Menu;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace House_of_Taste.Telas.Produtos
{
    public partial class ConsultarCardapio : Form
    {
        public ConsultarCardapio()
        {
            InitializeComponent();
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void label1_Click(object sender, EventArgs e)
        {
            TelaInicial menu = new TelaInicial();
            menu.Show();
            this.Close();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            Menu.TelaInicial menu = new Menu.TelaInicial();
            menu.Show();
            this.Close();
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            CadastroCardapio cadastrocardapio = new CadastroCardapio();
            cadastrocardapio.Show();
            this.Close();
        }

        private void CarregarDados()
        {
            CardapioBusiness business = new CardapioBusiness();
            dtg_consultarproduto.AutoGenerateColumns = false;
            dtg_consultarproduto.DataSource = business.Consultar(txtProduto.Text);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            CarregarDados();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            CardapioBusiness cardapiobusiness = new CardapioBusiness();
            CardapioDTO dto = dtg_consultarproduto.CurrentRow.DataBoundItem as CardapioDTO;
            cardapiobusiness.Alterar(dto);
            CarregarDados();
            MessageBox.Show("Os dados selecionados foram alterados.");
        }

        private void button3_Click(object sender, EventArgs e)
        {
            CardapioBusiness cardapiobusiness = new CardapioBusiness();
            CardapioDTO dto = dtg_consultarproduto.CurrentRow.DataBoundItem as CardapioDTO;
            cardapiobusiness.Remover(dto.Id_cardapio);
            CarregarDados();
            MessageBox.Show("Os dados selecionados foram deletados.");
        }

        private void dtg_consultarproduto_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void txtproduto_KeyPress(object sender, KeyPressEventArgs e)
        {
            //chama a business...
        }
    }
}
