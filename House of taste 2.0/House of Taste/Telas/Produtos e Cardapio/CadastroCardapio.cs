﻿using House_of_Taste.ClassesBDCardapio;
using House_of_Taste.ClassesBDFuncionario;
using House_of_Taste.Telas.Menu;
using House_of_Taste.Utilitarios;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace House_of_Taste.Telas.Produtos
{
    public partial class CadastroCardapio : Form
    {
        public CadastroCardapio()
        {
            InitializeComponent();
        }

        private void LimparCampos()
        {
            txtproduto.Text = string.Empty;
            txttipo.Text = string.Empty;
            vlproduto.Text = string.Empty;
        }

        private void label1_Click(object sender, EventArgs e)
        {
            TelaInicial menu = new TelaInicial();
            menu.Show();
            this.Close();
        }

        private void label5_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            Menu.TelaInicial menu = new Menu.TelaInicial();
            menu.Show();
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            ConsultarCardapio consultarcardapio = new ConsultarCardapio();
            consultarcardapio.Show();
            this.Close();
        }

        private void SalvarDados()
        {
            int fk = UserSession.UsuarioLogado.Id_funcionario;
            CardapioDTO dto = new CardapioDTO();
            dto.Nm_produto = txtproduto.Text;
            dto.tp_tipo = txttipo.Text;
            dto.Vl_valor = decimal.Parse(vlproduto.Text);
            dto.fk_funcinario = fk;
            CardapioBusiness salva = new CardapioBusiness();
            salva.Salvar(dto);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                exReg validar = new exReg();
                validar.ValidarValor(vlproduto.Text);
                validar.ValidarNome(txtproduto.Text);
                validar.ValidarNome(txttipo.Text);
                SalvarDados();
                MessageBox.Show("Dados salvos com sucesso.", "Salvo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Por favor, preencher corretamente todos os campos, não deixando nenhum vazio.", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
           
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            LimparCampos();
            MessageBox.Show("Campos cancelados.");
        }

        private void vlproduto_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void txtproduto_TextChanged(object sender, EventArgs e)
        {
           
        }
    }
}
