﻿using House_of_Taste.ClassesBDFuncionario;
using House_of_Taste.Utilitarios;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace House_of_Taste.Telas.Login
{
    public partial class ModificarSenha : Form
    {
        public ModificarSenha()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {
            Logar logardnv = new Logar();
            logardnv.Show();
            this.Close();

        }

        private void label5_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void txtusuario_TextChanged(object sender, EventArgs e)
        {
            if (txtcodigo.Text == codigo)
            {
                txtcodigo.BackColor = Color.LawnGreen;
            }
            else
            {
                txtcodigo.BackColor = Color.IndianRed;
               
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            FuncionarioBusiness funcionariobusiness = new FuncionarioBusiness();
            List<FuncionarioDTO> listar =     funcionariobusiness.Consultar(txtuser.Text);
            FuncionarioDTO dto = listar[0];
            SHA256Cript crip = new SHA256Cript();
            funcionariobusiness.AlterarSenha(dto.Id_funcionario, crip.Criptografar(txtsenha.Text));
            if (txtcodigo.Text == codigo)
            {
               
                MessageBox.Show("Senha modificada com sucesso.");
               
            }
            else
            {
             
                MessageBox.Show("Código inexistente, checar email corretamente.");
            }
        }


        CriacaodeCodigo cod = new CriacaodeCodigo();
        string codigo;

        private void button2_Click(object sender, EventArgs e)
        {
           
            if (txtsenha.Text != string.Empty && txtNovamente.Text != string.Empty && txtsenha.Text == txtNovamente.Text && txtemail.Text != string.Empty && txtuser.Text != string.Empty && txtemail.BackColor == Color.LawnGreen)
            {
                codigo = cod.Codigo4();
                GmailEmailService gmail = new GmailEmailService();
                EmailMessage msg = new EmailMessage();
                msg.Body = "O funcionario " + txtuser.Text + " está modificando sua senha de usuário." + " O código gerado para a modificação de senha é: " + codigo;
                msg.IsHtml = true;
                msg.Subject = "Código de Confirmação Senha";
                msg.ToEmail = txtemail.Text;
                gmail.SendEmailMessage(msg);
                MessageBox.Show("Código enviado com sucesso no email cadastrado.");
            }

            if (txtsenha.Text == string.Empty || txtNovamente.Text == string.Empty || txtemail.Text == string.Empty || txtuser.Text == string.Empty)
            {
                MessageBox.Show("Campos obrigatórios");
                
            }
            if (txtsenha.Text != txtNovamente.Text)
            {
                MessageBox.Show("Campos da senha diferentes. Por favor, digite a senha igual nos dois campos.");
            }
            if (txtemail.BackColor == Color.IndianRed)
            {
                MessageBox.Show("Email inválido");
            }
            if (txtsenha.BackColor == Color.IndianRed)
            {
                MessageBox.Show("Senhas inválidas, por favor, colocar letra maiúscula, minúscula, números e símbolos.");
            }


        }
        private void txtNovamente_TextChanged(object sender, EventArgs e)
        {
            if (txtNovamente.Text == txtsenha.Text)
            {
                txtNovamente.BackColor = Color.LawnGreen;
            }
            else
            {
                txtNovamente.BackColor = Color.IndianRed;
            }
        }



        private void txtemail_TextChanged(object sender, EventArgs e)
        {
            string email = txtemail.Text;

            Regex rg = new Regex(@"^[A-Za-z0-9](([_\.\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\.\-]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})$");

            bool validou = rg.IsMatch(txtemail.Text);
            if (validou == true)
            {
                txtemail.BackColor = Color.LawnGreen;
            }
            else
            {
                txtemail.BackColor = Color.IndianRed;
            }
        }

        private void nha()
        {
            string senha = txtsenha.Text;
            string ex = @"^.*(?=.{5,})(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=]).*$";
            Regex re = new Regex(ex);
            bool validar = re.IsMatch(txtsenha.Text);
            if (validar == true)
            {
                txtsenha.BackColor = Color.LawnGreen;
            }
            else
            {
                txtsenha.BackColor = Color.IndianRed;
            }
        }
        private void txtsenha_TextChanged(object sender, EventArgs e)
        {
            nha();
        }

        private void txtuser_TextChanged(object sender, EventArgs e)
        {
           
        }
    }
}


