﻿using House_of_Taste.ClassesBDFuncionario;
using House_of_Taste.Telas.Login;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace House_of_Taste.Telas
{
    public partial class Logar : Form
    {
        public Logar()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (txtusuario.Text == string.Empty)
            {
                MessageBox.Show("O campo de usuário é obrigatório para logar no sistema.");
            }

            if (txtsenha.Text == string.Empty)
            {
                MessageBox.Show("A senha é obrigatória para logar no sistema.");
            }

            FuncionarioBusiness business = new FuncionarioBusiness();

            SHA256Cript crip = new SHA256Cript();
            string senha = crip.Criptografar(txtsenha.Text);
            FuncionarioDTO funcionario = business.Login(txtusuario.Text, senha);
            if (funcionario != null)
            {
                UserSession.UsuarioLogado = funcionario;
                Telas.Menu.TelaInicial form = new Telas.Menu.TelaInicial();
                form.Show();
                this.Hide();
            }
            else
            {
                MessageBox.Show("Dados inválidos",
                                "Erro",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
            }

            
            
        }

        private void label2_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void label3_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void txtsenha_TextChanged(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {
            ModificarSenha modificarsenha = new ModificarSenha();
            modificarsenha.Show();
            this.Close();
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {

        }
    }
}
