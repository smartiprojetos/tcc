﻿using House_of_Taste.BancodeDados.ClassesBDEstoque;
using House_of_Taste.ClassesBDEstoque;
using House_of_Taste.ClassesBDFuncionario;
using House_of_Taste.Telas.Menu;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace House_of_Taste.Telas.Estoque
{
    public partial class ConsultarEstoque : Form
    {
        public ConsultarEstoque()
        {
            InitializeComponent();
        }

        private void label5_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void label1_Click(object sender, EventArgs e)
        {
            TelaInicial menu = new TelaInicial();
            menu.Show();
            this.Close();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            Menu.TelaInicial menu = new Menu.TelaInicial();
            menu.Show();
            this.Close();
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            CadastrarEstoque cadastrarestoque = new CadastrarEstoque();
            cadastrarestoque.Show();
            this.Close();
        }

        private void CarregarDados()
        {
            EstoqueBusiness business = new EstoqueBusiness();
            dtg_consultarproduto.AutoGenerateColumns = false;
            dtg_consultarproduto.DataSource = business.ConsultarEstoqueView(txtproduto.Text);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            CarregarDados();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            EstoqueBusiness produtobusiness = new EstoqueBusiness();
            ViewConsultarEstoqueDTO dto = dtg_consultarproduto.CurrentRow.DataBoundItem as ViewConsultarEstoqueDTO;
            produtobusiness.Remover(dto.IDestoque);
            CarregarDados();
            MessageBox.Show("Os dados selecionados foram deletados.");
 
        }

        private void button2_Click(object sender, EventArgs e)
        {
            int id = UserSession.UsuarioLogado.Id_funcionario;
            EstoqueBusiness estoquebusiness = new EstoqueBusiness();
            ViewConsultarEstoqueDTO dtoestoque = dtg_consultarproduto.CurrentRow.DataBoundItem as ViewConsultarEstoqueDTO;
            EstoqueDTO dto = new EstoqueDTO();
            dto.Id_estoque = dtoestoque.IDestoque;
            dto.FKIdProduto = dtoestoque.IDproduto;
            dto.IdFuncionarioParaEs = id;
            dto.valor_total = dtoestoque.ValorTotal;
            dto.retornovalor = dtoestoque.RetornoValor;
            dto.quantidade_saida = dtoestoque.QuantidadeSaida;
            dto.quantidade_entrada = dtoestoque.Quantidadeentrada;
            dto.data_saida = dtoestoque.DataSaida;
            dto.Data_entrada = dtoestoque.DataEntrada;
            estoquebusiness.Alterar(dto);
            CarregarDados();
            MessageBox.Show("Os dados selecionados foram alterados.");
        }
    }
}
