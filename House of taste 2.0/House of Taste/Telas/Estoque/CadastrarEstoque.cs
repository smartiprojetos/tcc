﻿using House_of_Taste.ClassesBDEstoque;
using House_of_Taste.ClassesBDFuncionario;
using House_of_Taste.ClassesBDProduto;
using House_of_Taste.Telas.Menu;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace House_of_Taste.Telas.Estoque
{
    public partial class CadastrarEstoque : Form
    {
        public CadastrarEstoque()
        {
            InitializeComponent();
            CarregarComboProduto();
        }

        private void Estoque_Load(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void label1_Click(object sender, EventArgs e)
        {
            TelaInicial menu = new TelaInicial();
            menu.Show();
            this.Close();
        }

        private void CarregarComboProduto()
        {
            ProdutoBusiness cadastroProduto = new ProdutoBusiness();
            List<ProdutoDTO> lista = cadastroProduto.Listar();
            cboproduto.ValueMember = nameof(ProdutoDTO.Id_cadastro_produto);
            cboproduto.DisplayMember = nameof(ProdutoDTO.nome_produto);
            cboproduto.DataSource = lista;
        }

        private void SalvarDados()
        {
            int pk = UserSession.UsuarioLogado.Id_funcionario;
            ProdutoDTO produto = cboproduto.SelectedItem as ProdutoDTO;
            EstoqueDTO dto = new EstoqueDTO();
            EstoqueBusiness business = new EstoqueBusiness();
            dto.FKIdProduto = produto.Id_cadastro_produto;
            dto.IdFuncionarioParaEs = pk;

            dto.quantidade_entrada = int.Parse(txtqntentrada.Text);
            dto.Data_entrada = dtpentrada.Value;
            dto.valor_total = int.Parse(txtvltotal.Text);
            dto.quantidade_saida = int.Parse(txtqntsaida.Text);
            dto.data_saida = dtpsaida.Value;
            dto.retornovalor = int.Parse(txtretorno.Text);
            business.Salvar(dto);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                SalvarDados();
                MessageBox.Show("Dados salvos com sucesso", "Salvo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception)
            {
                MessageBox.Show("Por Favor Insira os Dados correatamente", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            Menu.TelaInicial menu = new Menu.TelaInicial();
            menu.Show();
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ConsultarEstoque consultarestoque = new ConsultarEstoque();
            consultarestoque.Show();
            this.Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            txtqntentrada.Text = string.Empty;
            txtqntsaida.Text = string.Empty;
            txtretorno.Text = string.Empty;
            txtvltotal.Text = string.Empty;
        }
    }
}
