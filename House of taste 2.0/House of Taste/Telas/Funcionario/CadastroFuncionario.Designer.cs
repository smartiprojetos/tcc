﻿namespace House_of_Taste.Telas.Usuário
{
    partial class CadastroFuncionario
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CadastroFuncionario));
            this.txtconfimacaosenha = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.txtcpf = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.txtusuario = new System.Windows.Forms.TextBox();
            this.txtsenha = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtnome = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.dtkdata = new System.Windows.Forms.DateTimePicker();
            this.label7 = new System.Windows.Forms.Label();
            this.rdnadm = new System.Windows.Forms.RadioButton();
            this.button1 = new System.Windows.Forms.Button();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.rdnrh = new System.Windows.Forms.RadioButton();
            this.txtcargo = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.rdnrecepicionista = new System.Windows.Forms.RadioButton();
            this.rdnauxcozinha = new System.Windows.Forms.RadioButton();
            this.rdnauxlimpeza = new System.Windows.Forms.RadioButton();
            this.rdnfinancias = new System.Windows.Forms.RadioButton();
            this.rdngarcom = new System.Windows.Forms.RadioButton();
            this.rdnchefedecozinha = new System.Windows.Forms.RadioButton();
            this.rdnsempermissao = new System.Windows.Forms.RadioButton();
            this.button2 = new System.Windows.Forms.Button();
            this.label13 = new System.Windows.Forms.Label();
            this.txtemail = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtconfimacaosenha
            // 
            this.txtconfimacaosenha.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtconfimacaosenha.Location = new System.Drawing.Point(82, 258);
            this.txtconfimacaosenha.MaxLength = 16;
            this.txtconfimacaosenha.Name = "txtconfimacaosenha";
            this.txtconfimacaosenha.Size = new System.Drawing.Size(263, 20);
            this.txtconfimacaosenha.TabIndex = 4;
            this.txtconfimacaosenha.UseSystemPasswordChar = true;
            this.txtconfimacaosenha.TextChanged += new System.EventHandler(this.txtconfimacaosenha_TextChanged);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(107)))), ((int)(((byte)(69)))));
            this.label16.Location = new System.Drawing.Point(78, 235);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(149, 20);
            this.label16.TabIndex = 65;
            this.label16.Text = "Confirmar Senha:";
            // 
            // txtcpf
            // 
            this.txtcpf.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtcpf.Location = new System.Drawing.Point(82, 304);
            this.txtcpf.MaxLength = 16;
            this.txtcpf.Name = "txtcpf";
            this.txtcpf.Size = new System.Drawing.Size(263, 20);
            this.txtcpf.TabIndex = 5;
            this.txtcpf.TextChanged += new System.EventHandler(this.txtcpf_TextChanged);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(107)))), ((int)(((byte)(69)))));
            this.label15.Location = new System.Drawing.Point(78, 281);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(48, 20);
            this.label15.TabIndex = 63;
            this.label15.Text = "CPF:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(107)))), ((int)(((byte)(69)))));
            this.label11.Location = new System.Drawing.Point(78, 327);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(131, 20);
            this.label11.TabIndex = 62;
            this.label11.Text = "Data Cadastro:";
            // 
            // txtusuario
            // 
            this.txtusuario.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtusuario.Location = new System.Drawing.Point(82, 166);
            this.txtusuario.MaxLength = 30;
            this.txtusuario.Name = "txtusuario";
            this.txtusuario.Size = new System.Drawing.Size(263, 20);
            this.txtusuario.TabIndex = 2;
            this.txtusuario.TextChanged += new System.EventHandler(this.textBox3_TextChanged);
            // 
            // txtsenha
            // 
            this.txtsenha.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtsenha.Location = new System.Drawing.Point(82, 212);
            this.txtsenha.MaxLength = 16;
            this.txtsenha.Name = "txtsenha";
            this.txtsenha.Size = new System.Drawing.Size(263, 20);
            this.txtsenha.TabIndex = 3;
            this.txtsenha.UseSystemPasswordChar = true;
            this.txtsenha.TextChanged += new System.EventHandler(this.txtsenha_TextChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(107)))), ((int)(((byte)(69)))));
            this.label6.Location = new System.Drawing.Point(78, 189);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(66, 20);
            this.label6.TabIndex = 59;
            this.label6.Text = "Senha:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Century Gothic", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(107)))), ((int)(((byte)(69)))));
            this.label5.Location = new System.Drawing.Point(597, -9);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(24, 28);
            this.label5.TabIndex = 58;
            this.label5.Text = "_";
            this.label5.Click += new System.EventHandler(this.label5_Click);
            // 
            // txtnome
            // 
            this.txtnome.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtnome.Location = new System.Drawing.Point(82, 120);
            this.txtnome.MaxLength = 50;
            this.txtnome.Name = "txtnome";
            this.txtnome.Size = new System.Drawing.Size(263, 20);
            this.txtnome.TabIndex = 1;
            this.txtnome.TextChanged += new System.EventHandler(this.txtnome_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(107)))), ((int)(((byte)(69)))));
            this.label4.Location = new System.Drawing.Point(78, 143);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(52, 20);
            this.label4.TabIndex = 56;
            this.label4.Text = "User:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(107)))), ((int)(((byte)(69)))));
            this.label3.Location = new System.Drawing.Point(78, 97);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(141, 20);
            this.label3.TabIndex = 55;
            this.label3.Text = "Nome Completo:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(107)))), ((int)(((byte)(69)))));
            this.label2.Location = new System.Drawing.Point(218, 43);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(295, 32);
            this.label2.TabIndex = 54;
            this.label2.Text = "Cadastro Funcionário";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(107)))), ((int)(((byte)(69)))));
            this.label1.Location = new System.Drawing.Point(626, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(24, 25);
            this.label1.TabIndex = 53;
            this.label1.Text = "x";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // dtkdata
            // 
            this.dtkdata.Location = new System.Drawing.Point(82, 350);
            this.dtkdata.Name = "dtkdata";
            this.dtkdata.Size = new System.Drawing.Size(200, 20);
            this.dtkdata.TabIndex = 6;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(107)))), ((int)(((byte)(69)))));
            this.label7.Location = new System.Drawing.Point(401, 97);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(150, 20);
            this.label7.TabIndex = 68;
            this.label7.Text = "Permissão/Cargo:";
            // 
            // rdnadm
            // 
            this.rdnadm.AutoSize = true;
            this.rdnadm.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdnadm.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(107)))), ((int)(((byte)(69)))));
            this.rdnadm.Location = new System.Drawing.Point(405, 144);
            this.rdnadm.Name = "rdnadm";
            this.rdnadm.Size = new System.Drawing.Size(119, 20);
            this.rdnadm.TabIndex = 69;
            this.rdnadm.TabStop = true;
            this.rdnadm.Text = "Administrador";
            this.rdnadm.UseVisualStyleBackColor = true;
            this.rdnadm.CheckedChanged += new System.EventHandler(this.radioButton1_CheckedChanged);
            // 
            // button1
            // 
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(107)))), ((int)(((byte)(69)))));
            this.button1.Location = new System.Drawing.Point(401, 431);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(125, 37);
            this.button1.TabIndex = 75;
            this.button1.Text = "Cadastrar";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(107)))), ((int)(((byte)(69)))));
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(0, 0);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(59, 57);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 10;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Click += new System.EventHandler(this.pictureBox2_Click);
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
            this.pictureBox3.Location = new System.Drawing.Point(-144, 97);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(364, 283);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 11;
            this.pictureBox3.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(107)))), ((int)(((byte)(69)))));
            this.panel1.Controls.Add(this.pictureBox2);
            this.panel1.Controls.Add(this.pictureBox3);
            this.panel1.Location = new System.Drawing.Point(-1, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(57, 679);
            this.panel1.TabIndex = 76;
            // 
            // rdnrh
            // 
            this.rdnrh.AutoSize = true;
            this.rdnrh.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdnrh.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(107)))), ((int)(((byte)(69)))));
            this.rdnrh.Location = new System.Drawing.Point(405, 169);
            this.rdnrh.Name = "rdnrh";
            this.rdnrh.Size = new System.Drawing.Size(43, 20);
            this.rdnrh.TabIndex = 77;
            this.rdnrh.TabStop = true;
            this.rdnrh.Text = "RH";
            this.rdnrh.UseVisualStyleBackColor = true;
            this.rdnrh.CheckedChanged += new System.EventHandler(this.radioButton1_CheckedChanged_1);
            // 
            // txtcargo
            // 
            this.txtcargo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtcargo.Location = new System.Drawing.Point(82, 396);
            this.txtcargo.MaxLength = 45;
            this.txtcargo.Name = "txtcargo";
            this.txtcargo.Size = new System.Drawing.Size(263, 20);
            this.txtcargo.TabIndex = 7;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(107)))), ((int)(((byte)(69)))));
            this.label12.Location = new System.Drawing.Point(78, 373);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(62, 20);
            this.label12.TabIndex = 78;
            this.label12.Text = "Cargo:";
            this.label12.Click += new System.EventHandler(this.label12_Click);
            // 
            // rdnrecepicionista
            // 
            this.rdnrecepicionista.AutoSize = true;
            this.rdnrecepicionista.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdnrecepicionista.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(107)))), ((int)(((byte)(69)))));
            this.rdnrecepicionista.Location = new System.Drawing.Point(405, 247);
            this.rdnrecepicionista.Name = "rdnrecepicionista";
            this.rdnrecepicionista.Size = new System.Drawing.Size(118, 20);
            this.rdnrecepicionista.TabIndex = 80;
            this.rdnrecepicionista.TabStop = true;
            this.rdnrecepicionista.Text = "Recepcionista";
            this.rdnrecepicionista.UseVisualStyleBackColor = true;
            // 
            // rdnauxcozinha
            // 
            this.rdnauxcozinha.AutoSize = true;
            this.rdnauxcozinha.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdnauxcozinha.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(107)))), ((int)(((byte)(69)))));
            this.rdnauxcozinha.Location = new System.Drawing.Point(405, 221);
            this.rdnauxcozinha.Name = "rdnauxcozinha";
            this.rdnauxcozinha.Size = new System.Drawing.Size(108, 20);
            this.rdnauxcozinha.TabIndex = 81;
            this.rdnauxcozinha.TabStop = true;
            this.rdnauxcozinha.Text = "Aux Cozinha";
            this.rdnauxcozinha.UseVisualStyleBackColor = true;
            this.rdnauxcozinha.CheckedChanged += new System.EventHandler(this.rdnauxcozinha_CheckedChanged);
            // 
            // rdnauxlimpeza
            // 
            this.rdnauxlimpeza.AutoSize = true;
            this.rdnauxlimpeza.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdnauxlimpeza.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(107)))), ((int)(((byte)(69)))));
            this.rdnauxlimpeza.Location = new System.Drawing.Point(405, 195);
            this.rdnauxlimpeza.Name = "rdnauxlimpeza";
            this.rdnauxlimpeza.Size = new System.Drawing.Size(109, 20);
            this.rdnauxlimpeza.TabIndex = 82;
            this.rdnauxlimpeza.TabStop = true;
            this.rdnauxlimpeza.Text = "Aux Limpeza";
            this.rdnauxlimpeza.UseVisualStyleBackColor = true;
            // 
            // rdnfinancias
            // 
            this.rdnfinancias.AutoSize = true;
            this.rdnfinancias.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdnfinancias.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(107)))), ((int)(((byte)(69)))));
            this.rdnfinancias.Location = new System.Drawing.Point(405, 299);
            this.rdnfinancias.Name = "rdnfinancias";
            this.rdnfinancias.Size = new System.Drawing.Size(84, 20);
            this.rdnfinancias.TabIndex = 84;
            this.rdnfinancias.TabStop = true;
            this.rdnfinancias.Text = "Finanças";
            this.rdnfinancias.UseVisualStyleBackColor = true;
            // 
            // rdngarcom
            // 
            this.rdngarcom.AutoSize = true;
            this.rdngarcom.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdngarcom.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(107)))), ((int)(((byte)(69)))));
            this.rdngarcom.Location = new System.Drawing.Point(405, 273);
            this.rdngarcom.Name = "rdngarcom";
            this.rdngarcom.Size = new System.Drawing.Size(79, 20);
            this.rdngarcom.TabIndex = 85;
            this.rdngarcom.TabStop = true;
            this.rdngarcom.Text = "Garçom";
            this.rdngarcom.UseVisualStyleBackColor = true;
            // 
            // rdnchefedecozinha
            // 
            this.rdnchefedecozinha.AutoSize = true;
            this.rdnchefedecozinha.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdnchefedecozinha.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(107)))), ((int)(((byte)(69)))));
            this.rdnchefedecozinha.Location = new System.Drawing.Point(405, 325);
            this.rdnchefedecozinha.Name = "rdnchefedecozinha";
            this.rdnchefedecozinha.Size = new System.Drawing.Size(142, 20);
            this.rdnchefedecozinha.TabIndex = 86;
            this.rdnchefedecozinha.TabStop = true;
            this.rdnchefedecozinha.Text = "Chefe de Cozinha";
            this.rdnchefedecozinha.UseVisualStyleBackColor = true;
            // 
            // rdnsempermissao
            // 
            this.rdnsempermissao.AutoSize = true;
            this.rdnsempermissao.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdnsempermissao.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(107)))), ((int)(((byte)(69)))));
            this.rdnsempermissao.Location = new System.Drawing.Point(405, 350);
            this.rdnsempermissao.Name = "rdnsempermissao";
            this.rdnsempermissao.Size = new System.Drawing.Size(122, 20);
            this.rdnsempermissao.TabIndex = 87;
            this.rdnsempermissao.TabStop = true;
            this.rdnsempermissao.Text = "Sem Permissão";
            this.rdnsempermissao.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(107)))), ((int)(((byte)(69)))));
            this.button2.Location = new System.Drawing.Point(532, 431);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(118, 37);
            this.button2.TabIndex = 88;
            this.button2.Text = "Consultar";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(107)))), ((int)(((byte)(69)))));
            this.label13.Location = new System.Drawing.Point(78, 425);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(58, 20);
            this.label13.TabIndex = 90;
            this.label13.Text = "Email:";
            // 
            // txtemail
            // 
            this.txtemail.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtemail.Location = new System.Drawing.Point(84, 448);
            this.txtemail.MaxLength = 45;
            this.txtemail.Name = "txtemail";
            this.txtemail.Size = new System.Drawing.Size(261, 20);
            this.txtemail.TabIndex = 89;
            // 
            // CadastroFuncionario
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(657, 482);
            this.Controls.Add(this.txtemail);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.rdnsempermissao);
            this.Controls.Add(this.rdnchefedecozinha);
            this.Controls.Add(this.rdngarcom);
            this.Controls.Add(this.rdnfinancias);
            this.Controls.Add(this.rdnauxlimpeza);
            this.Controls.Add(this.rdnauxcozinha);
            this.Controls.Add(this.rdnrecepicionista);
            this.Controls.Add(this.txtcargo);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.rdnrh);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.rdnadm);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.dtkdata);
            this.Controls.Add(this.txtconfimacaosenha);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.txtcpf);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.txtusuario);
            this.Controls.Add(this.txtsenha);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtnome);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "CadastroFuncionario";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "CadastroFuncionario";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtconfimacaosenha;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtcpf;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtusuario;
        private System.Windows.Forms.TextBox txtsenha;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtnome;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker dtkdata;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.RadioButton rdnadm;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.RadioButton rdnrh;
        private System.Windows.Forms.TextBox txtcargo;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.RadioButton rdnrecepicionista;
        private System.Windows.Forms.RadioButton rdnauxcozinha;
        private System.Windows.Forms.RadioButton rdnauxlimpeza;
        private System.Windows.Forms.RadioButton rdnfinancias;
        private System.Windows.Forms.RadioButton rdngarcom;
        private System.Windows.Forms.RadioButton rdnchefedecozinha;
        private System.Windows.Forms.RadioButton rdnsempermissao;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtemail;
    }
}