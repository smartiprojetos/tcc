﻿using House_of_Taste.ClassesBDFuncionario;
using House_of_Taste.Telas.Menu;
using House_of_Taste.Utilitarios;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace House_of_Taste.Telas.Usuário
{
    public partial class CadastroFuncionario : Form
    {
        public CadastroFuncionario()
        {
            InitializeComponent();
        }

        private void label5_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void label1_Click(object sender, EventArgs e)
        {
            TelaInicial menu = new TelaInicial();
            menu.Show();
            this.Close();
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            { 
            if (txtnome.Text == string.Empty)
                MessageBox.Show("O campo nome não pode estar em branco.");
            if (txtusuario.Text == string.Empty)
                MessageBox.Show("O campo usuário não pode estar em branco.");
            if (txtsenha.Text == string.Empty)
                MessageBox.Show("O campo senha não pode estar em branco.");
            if (txtconfimacaosenha.Text == string.Empty)
                MessageBox.Show("O campo de confirmar sua senha não pode estar em branco.");
            if (txtcpf.Text == string.Empty)
                MessageBox.Show("O campo do CPF não pode estar em branco.");

            Regex regra1 = new Regex(@"^[A-Za-z ]{0,}$");
            if (regra1.IsMatch(txtnome.Text) == false)
                MessageBox.Show("O nome pode conter apenas letras e espaços.");
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            FuncionarioDTO dto = new FuncionarioDTO();
            dto.nomecompleto = txtnome.Text;
            dto.usuario = txtusuario.Text;
            dto.cargo = txtcargo.Text;
            dto.cpf = txtcpf.Text;
            dto.email = txtemail.Text;
            if (txtsenha.Text == txtconfimacaosenha.Text)
            {
                SHA256Cript crip = new SHA256Cript();
                dto.senha = crip.Criptografar(txtsenha.Text);
            }
            else
            {
                MessageBox.Show("Senha incorreta, por favor conferir", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            dto.cadastro = dtkdata.Value;
            if (rdnadm.Checked == true)
            {
                dto.permissao_funcionarios = true;
                dto.permissao_produtos = true;
                dto.permissao_cardapio = true;
                dto.permissao_fornecedor = true;
                dto.permissao_cadastrar = true;
                dto.permissao_estoque = true;
                dto.permissao_atualizar_estoque = true;
                dto.permissao_consultar_estoque = true;
                dto.permissao_pedido_fornecedor = true;
                dto.permissao_cliente = true;
                dto.permissao_controle_cliente = true;
                dto.permissao_consultar_cliente = true;
                dto.permissao_cadastrar_funcionarios = true;
                dto.permissao_consultar_funcionarios = true;
                dto.permissao_cadastrar_novo_produto = true;
                dto.consultar_produtos = true;
                dto.permissao_consultar_cardapio = true;
                dto.permissao_cadastro_novo_cardapio = true;
                dto.permissao_novo_fornecedor = true;
                dto.permissao_consultar_fornecedor = true;
            }

            if (rdnsempermissao.Checked == true)
            {
                dto.permissao_funcionarios = false;
                dto.permissao_produtos = false;
                dto.permissao_cardapio = false;
                dto.permissao_fornecedor = false;
                dto.permissao_cadastrar = false;
                dto.permissao_estoque = false;
                dto.permissao_atualizar_estoque = false;
                dto.permissao_consultar_estoque = false;
                dto.permissao_pedido_fornecedor = false;
                dto.permissao_cliente = false;
                dto.permissao_controle_cliente = false;
                dto.permissao_consultar_cliente = false;
                dto.permissao_cadastrar_funcionarios = false;
                dto.permissao_consultar_funcionarios = false;
                dto.permissao_cadastrar_novo_produto = false;
                dto.consultar_produtos = false;
                dto.permissao_consultar_cardapio = false;
                dto.permissao_cadastro_novo_cardapio = false;
                dto.permissao_novo_fornecedor = false;
                dto.permissao_consultar_fornecedor = false;
            }
            if(rdnrh.Checked==true)
            {
                dto.permissao_cadastrar = true;
                dto.permissao_funcionarios = true;
                dto.permissao_cadastrar_funcionarios = true;
                dto.permissao_consultar_funcionarios = true;

                dto.permissao_produtos = false;
                dto.permissao_cardapio = false;
                dto.permissao_fornecedor = false;
                dto.permissao_estoque = false;
                dto.permissao_atualizar_estoque = false;
                dto.permissao_consultar_estoque = false;
                dto.permissao_pedido_fornecedor = false;
                dto.permissao_cliente = false;
                dto.permissao_controle_cliente = false;
                dto.permissao_consultar_cliente = false;
                dto.permissao_cadastrar_novo_produto = false;
                dto.consultar_produtos = false;
                dto.permissao_consultar_cardapio = false;
                dto.permissao_cadastro_novo_cardapio = false;
                dto.permissao_novo_fornecedor = false;
                dto.permissao_consultar_fornecedor = false;
            }
             if(rdnauxlimpeza.Checked==true)
             {
                
                dto.permissao_atualizar_estoque = true;
                dto.permissao_consultar_estoque = true;
                dto.permissao_estoque = true;

                dto.permissao_cadastrar = false; ;
                dto.permissao_consultar_cardapio = false;
                dto.permissao_cadastro_novo_cardapio = false;
                dto.permissao_novo_fornecedor = false;
                dto.permissao_consultar_fornecedor = false;
                dto.permissao_funcionarios = false;
                dto.permissao_produtos = false;
                dto.permissao_cardapio = false;
                dto.permissao_fornecedor = false;
                dto.permissao_produtos = false;
                dto.consultar_produtos = false;
                dto.permissao_pedido_fornecedor = false;
                dto.permissao_cliente = false;
                dto.permissao_controle_cliente = false;
                dto.permissao_consultar_cliente = false;
                dto.permissao_cadastrar_funcionarios = false;
                dto.permissao_consultar_funcionarios = false;
                dto.permissao_cadastrar_novo_produto = false;
             }

             if(rdnauxcozinha.Checked==true)
             {
                dto.permissao_cadastrar = true;
                dto.permissao_produtos = true;
                dto.consultar_produtos = true;
                dto.permissao_cadastrar_novo_produto = true;
                dto.permissao_cardapio = true;
                dto.permissao_consultar_cardapio = true;
                dto.permissao_cadastro_novo_cardapio = true;
                dto.permissao_estoque =true;
                dto.permissao_atualizar_estoque = true;
                dto.permissao_consultar_estoque = true;

                dto.permissao_novo_fornecedor = false;
                dto.permissao_consultar_fornecedor = false;
                dto.permissao_funcionarios = false;
                dto.permissao_fornecedor = false;
                dto.permissao_pedido_fornecedor = false;
                dto.permissao_cliente = false;
                dto.permissao_controle_cliente = false;
                dto.permissao_consultar_cliente = false;
                dto.permissao_cadastrar_funcionarios = false;
                dto.permissao_consultar_funcionarios = false;
             }

             if(rdnchefedecozinha.Checked==true)
             {
                dto.permissao_cadastrar = true;
                dto.permissao_produtos = true;
                dto.consultar_produtos = true;
                dto.permissao_cadastrar_novo_produto = true;
                dto.permissao_cardapio = true;
                dto.permissao_consultar_cardapio = true;
                dto.permissao_cadastro_novo_cardapio = true;
                dto.permissao_estoque = true;
                dto.permissao_atualizar_estoque = true;
                dto.permissao_consultar_estoque = true;

                dto.permissao_novo_fornecedor = false;
                dto.permissao_consultar_fornecedor = false;
                dto.permissao_funcionarios = false;
                dto.permissao_fornecedor = false;
                dto.permissao_pedido_fornecedor = false;
                dto.permissao_cliente = false;
                dto.permissao_controle_cliente = false;
                dto.permissao_consultar_cliente = false;
                dto.permissao_cadastrar_funcionarios = false;
                dto.permissao_consultar_funcionarios = false;
             }

             if(rdnrecepicionista.Checked==true)
             {
                dto.permissao_cliente = true;
                dto.permissao_controle_cliente = true;
                dto.permissao_consultar_cliente = true;

                dto.permissao_funcionarios = false;
                dto.permissao_produtos = false;
                dto.permissao_cardapio = false;
                dto.permissao_fornecedor = false;
                dto.permissao_cadastrar = false;
                dto.permissao_estoque = false;
                dto.permissao_atualizar_estoque = false;
                dto.permissao_consultar_estoque = false;
                dto.permissao_pedido_fornecedor = false;
                dto.permissao_cadastrar_novo_produto = false;
                dto.consultar_produtos = false;
                dto.permissao_consultar_cardapio = false;
                dto.permissao_cadastro_novo_cardapio = false;
                dto.permissao_novo_fornecedor = false;
                dto.permissao_consultar_fornecedor = false;
             }

             if(rdngarcom.Checked==true)
             {
                dto.permissao_cadastrar = true;
                dto.permissao_cliente = true;
                dto.permissao_controle_cliente = true;
                dto.permissao_consultar_cliente = true;

                dto.permissao_funcionarios = false;
                dto.permissao_produtos = false;
                dto.permissao_cardapio = false;
                dto.permissao_fornecedor = false;
                dto.permissao_estoque = false;
                dto.permissao_atualizar_estoque = false;
                dto.permissao_consultar_estoque = false;
                dto.permissao_pedido_fornecedor = false;
                dto.permissao_cadastrar_novo_produto = false;
                dto.consultar_produtos = false;
                dto.permissao_consultar_cardapio = false;
                dto.permissao_cadastro_novo_cardapio = false;
                dto.permissao_novo_fornecedor = false;
                dto.permissao_consultar_fornecedor = false;
             }

             if(rdnfinancias.Checked==true)
             {
                dto.permissao_cadastrar = true;
                dto.permissao_estoque = true;
                dto.permissao_atualizar_estoque = true;
                dto.permissao_consultar_estoque = true;
                dto.permissao_fornecedor = true;
                dto.permissao_novo_fornecedor = true;
                dto.permissao_consultar_fornecedor = true;
                dto.permissao_funcionarios = true;
                dto.permissao_cadastrar_funcionarios = true;
                dto.permissao_consultar_funcionarios = true;
                dto.permissao_cliente = true;
                dto.permissao_controle_cliente = true;
                dto.permissao_consultar_cliente = true;

                dto.consultar_produtos = false;
                dto.permissao_cadastrar_novo_produto = false;
                dto.consultar_produtos = false;
                dto.permissao_cardapio = false;
                dto.permissao_consultar_cardapio = false;
                dto.permissao_cadastro_novo_cardapio = false;
            }

            if (rdnadm.Checked == false && rdnrh.Checked == false && rdnauxcozinha.Checked==false && rdnauxcozinha.Checked && rdnauxlimpeza.Checked==false && rdnchefedecozinha.Checked==false && rdnfinancias.Checked==false && rdngarcom.Checked && rdnrecepicionista.Checked && rdnsempermissao.Checked)
            {
                MessageBox.Show("Por favor marque um dos campos referente as permissões", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


            FuncionarioBusiness business = new FuncionarioBusiness();
            business.Salvar(dto);
            MessageBox.Show("Dados salvos com sucesso", "Infomation", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void radioButton1_CheckedChanged_1(object sender, EventArgs e)
        {
            
        }

        private void rdnauxcozinha_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            Menu.TelaInicial menu = new Menu.TelaInicial();
            menu.Show();
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            ConsultarFuncionarios consultarfuncionarios = new ConsultarFuncionarios();
            consultarfuncionarios.Show();
            this.Close();
        }

        private void txtcpf_TextChanged(object sender, EventArgs e)
        {
            string cpf = txtcpf.Text;

            Regex rg = new Regex(@"^\d{3}\.\d{3}\.\d{3}-\d{2}$");

            bool validou = rg.IsMatch(txtcpf.Text);
            if (validou == true)
            {
                txtcpf.BackColor = Color.LawnGreen;
            }
            else
            {
                txtcpf.BackColor = Color.IndianRed;
            }

            
        }

        private void textBox5_TextChanged(object sender, EventArgs e)
        {
          
        }

        private void txtnome_TextChanged(object sender, EventArgs e)
        {
            Regex regra1 = new Regex(@"^[A-Za-z ]{0,}$");
        }

        private void txtconfimacaosenha_TextChanged(object sender, EventArgs e)
        {
            if (txtconfimacaosenha.Text == txtsenha.Text)
            {
                txtconfimacaosenha.BackColor = Color.LawnGreen;
            }
            else
            {
                txtconfimacaosenha.BackColor = Color.IndianRed;
            }
        }

        private void txtsenha_TextChanged(object sender, EventArgs e)
        {
            string senha = txtsenha.Text;
            string ex = @"^.*(?=.{5,})(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=]).*$";
            Regex re = new Regex(ex);
            bool validar = re.IsMatch(txtsenha.Text);
            if (validar == true)
            {
                txtsenha.BackColor = Color.LawnGreen;
            }
            else
            {
                txtsenha.BackColor = Color.IndianRed;
            }
        }

        private void label12_Click(object sender, EventArgs e)
        {
            //colocar no lugar de cargo o email
        }
    }
}
