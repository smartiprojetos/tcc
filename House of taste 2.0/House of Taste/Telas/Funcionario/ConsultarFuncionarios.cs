﻿using House_of_Taste.ClassesBDFuncionario;
using House_of_Taste.Telas.Menu;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace House_of_Taste.Telas.Usuário
{
    public partial class ConsultarFuncionarios : Form
    {
        public ConsultarFuncionarios()
        {
            InitializeComponent();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            Menu.TelaInicial menu = new Menu.TelaInicial();
            menu.Show();
            this.Close();
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void label1_Click(object sender, EventArgs e)
        {
            TelaInicial menu = new TelaInicial();
            menu.Show();
            this.Close();
        }

        private void ConsultarFuncionarios_Load(object sender, EventArgs e)
        {

        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            CadastroFuncionario cadastrofuncionario = new CadastroFuncionario();
            cadastrofuncionario.Show();
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            CarregarDados();

        }

        private void CarregarDados()
        {
            FuncionarioBusiness business = new FuncionarioBusiness();
            dtg_consultarproduto.AutoGenerateColumns = false;
            dtg_consultarproduto.DataSource = business.Consultar(txtusuario.Text);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            FuncionarioBusiness funcionariobusiness = new FuncionarioBusiness();
            FuncionarioDTO dto = dtg_consultarproduto.CurrentRow.DataBoundItem as FuncionarioDTO;
            funcionariobusiness.Alterar(dto);
            CarregarDados();
            MessageBox.Show("Os dados selecionados foram alterados.");
        }

        private void button3_Click(object sender, EventArgs e)
        {
            FuncionarioBusiness funcionariobusiness = new FuncionarioBusiness();
            FuncionarioDTO dto = dtg_consultarproduto.CurrentRow.DataBoundItem as FuncionarioDTO;
            funcionariobusiness.Remover(dto.Id_funcionario);
            CarregarDados();
            MessageBox.Show("Os dados selecionados foram deletados.");
        }

        private void dtg_consultarproduto_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
