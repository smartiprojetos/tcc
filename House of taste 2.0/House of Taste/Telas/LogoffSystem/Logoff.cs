﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace House_of_Taste.Telas.LogoffSystem
{
    public partial class Logoff : Form
    {
        public Logoff()
        {
            InitializeComponent();
            InitializeComponent();
            Task.Factory.StartNew(() =>
            {
                System.Threading.Thread.Sleep(3000);
                
                    Application.Exit();
            });
        }
    }
}
