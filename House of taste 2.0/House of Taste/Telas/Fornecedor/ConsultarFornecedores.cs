﻿using House_of_Taste.ClassesBDFornecedor;
using House_of_Taste.Telas.Menu;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace House_of_Taste.Telas.Fornecedor
{
    public partial class ConsultarFornecedores : Form
    {
        public ConsultarFornecedores()
        {
            InitializeComponent();
        }

        private void ConsultarFornecedores_Load(object sender, EventArgs e)
        {

        }

        private void dtg_consultarproduto_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
           
        }

        private void button1_Click(object sender, EventArgs e)
        {
            CarregarDados();
            


        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            Menu.TelaInicial menu = new Menu.TelaInicial();
            menu.Show();
            this.Close();
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void label1_Click(object sender, EventArgs e)
        {
            TelaInicial menu = new TelaInicial();
            menu.Show();
            this.Close();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            CadastroFornecedores cadastrofornecedores = new CadastroFornecedores();
            cadastrofornecedores.Show();
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            FiltrarEndereco filtrarendereco = new FiltrarEndereco();
            filtrarendereco.Show();
            this.Close();
        }

        private void CarregarDados()
        {
            FornecedorBusiness business = new FornecedorBusiness();
            dtg_consultarproduto.AutoGenerateColumns = false;
            List<FornecedorDTO> lista = business.Listar();
            dtg_consultarproduto.DataSource = lista;

        }

        private void button3_Click(object sender, EventArgs e)
        {
            FornecedorBusiness Fornecedorbusiness = new FornecedorBusiness();
            FornecedorDTO dto = dtg_consultarproduto.CurrentRow.DataBoundItem as FornecedorDTO;
            Fornecedorbusiness.Remover(dto.Id_fornecedor);
            CarregarDados();
            MessageBox.Show("Os dados selecionados foram deletados.");
        }
    }
}
