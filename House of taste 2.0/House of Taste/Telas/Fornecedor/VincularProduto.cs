﻿using House_of_Taste.BancodeDados.ProdutoFornecedor;
using House_of_Taste.ClassesBDFornecedor;
using House_of_Taste.ClassesBDProduto;
using House_of_Taste.Telas.Menu;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace House_of_Taste.Telas.Fornecedor
{
    public partial class VincularProduto : Form
    {
        BindingList<DTONomedeProdutoeFornecedor> listadagrid = new BindingList<DTONomedeProdutoeFornecedor>();
        public VincularProduto()
        {
            InitializeComponent();
            CarregarComboProduto();
            CarregarComboFornecedor();
            ConfigurarGrid();
        }

        void CarregarComboProduto()
        {
            ProdutoBusiness cadastroProduto = new ProdutoBusiness();
            List<ProdutoDTO> dto = cadastroProduto.Listar();

            cboProduto.ValueMember = nameof(ProdutoDTO.Id_cadastro_produto);
            cboProduto.DisplayMember = nameof(ProdutoDTO.nome_produto);
            cboProduto.DataSource = dto;
        }

        void CarregarComboFornecedor()
        {
            FornecedorBusiness fornecedores = new FornecedorBusiness();
            List<FornecedorDTO> dto = fornecedores.Listar();

            cboFornecedor.ValueMember = nameof(FornecedorDTO.Id_fornecedor);
            cboFornecedor.DisplayMember = nameof(FornecedorDTO.Nm_empresa);

            cboFornecedor.DataSource = dto;
        }

        void ConfigurarGrid()
        {
            dtg_consultarproduto.AutoGenerateColumns = false;
            dtg_consultarproduto.DataSource = listadagrid;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ProdutoDTO dto = cboProduto.SelectedItem as ProdutoDTO;
            FornecedorDTO fornecedores = cboFornecedor.SelectedItem as FornecedorDTO;
            DTONomedeProdutoeFornecedor produtoeFornecedor = new DTONomedeProdutoeFornecedor();

            produtoeFornecedor.IdProduto = dto.Id_cadastro_produto;
            produtoeFornecedor.NomeProduto = dto.nome_produto;
            produtoeFornecedor.IdFornecedor = fornecedores.Id_fornecedor;
            produtoeFornecedor.NomeFornecedor = fornecedores.Nm_empresa;

            listadagrid.Add(produtoeFornecedor);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                DTONomedeProdutoeFornecedor produtoeFornecedor = new DTONomedeProdutoeFornecedor();
                ProdutoFornecedorBusiness business = new ProdutoFornecedorBusiness();
                business.Salvar(listadagrid.ToList());
                MessageBox.Show("Dados salvos com sucesso.", "House of Taste", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception)
            {
                MessageBox.Show("Por favor inserir os dados corretamente.", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void label2_Click(object sender, EventArgs e)
        {
            TelaInicial menu = new TelaInicial();
            menu.Show();
            this.Close();
        }

        private void label5_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void label5_Click_1(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            CadastroFornecedores cadastrofornecedores = new CadastroFornecedores();
            cadastrofornecedores.Show();
            this.Close();
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            Menu.TelaInicial menu = new Menu.TelaInicial();
            menu.Show();
            this.Close();
        }
    }
}
