﻿using House_of_Taste.BancodeDados.ClassesBDPedidoFornecedor;
using House_of_Taste.BancodeDados.ProdutoFornecedor;
using House_of_Taste.ClassesBDFornecedor;
using House_of_Taste.ClassesBDFuncionario;
using House_of_Taste.ClassesBDPedidoFornecedor;
using House_of_Taste.ClassesBDProduto;
using House_of_Taste.Telas.Menu;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace House_of_Taste.Telas.Fornecedor
{
    public partial class PedidoFornecedor : Form
    {
        BindingList<PedidoparaFonecedorGridDTO> produtosfornecedor = new BindingList<PedidoparaFonecedorGridDTO>();
        List<FornecedorDTO> lista1 = new List<FornecedorDTO>();
        DTONomedeProdutoeFornecedor produtodto = new DTONomedeProdutoeFornecedor();
        FornecedorDTO fornecedoresdto = new FornecedorDTO();
        int pk;
        public PedidoFornecedor()
        {
            InitializeComponent();
            CarregarComboFornecedor();
            Grid();
        }
        decimal preco, total;


        private void label5_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void label2_Click(object sender, EventArgs e)
        {
            TelaInicial menu = new TelaInicial();
            menu.Show();
            this.Close();
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            Menu.TelaInicial menu = new Menu.TelaInicial();
            menu.Show();
            this.Close();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }


        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            fornecedoresdto = cboFornecedor.SelectedItem as FornecedorDTO;
            pk = fornecedoresdto.Id_fornecedor;

            ProdutoFornecedorBusiness business = new ProdutoFornecedorBusiness();
            cboProduto.ValueMember = nameof(DTONomedeProdutoeFornecedor.IDprodutoFornecedor);
            cboProduto.DisplayMember = nameof(DTONomedeProdutoeFornecedor.NomeProduto);
            List<DTONomedeProdutoeFornecedor> lista = business.Consultar(pk);
            cboProduto.DataSource = lista;
        }

        private void ColocarDadosNagrid()
        {
            produtodto = cboProduto.SelectedItem as DTONomedeProdutoeFornecedor;
            PedidoparaFonecedorGridDTO pedidofornecedor = new PedidoparaFonecedorGridDTO();

            if (txtquantidade.Text != null)
            {

                pedidofornecedor.NomeProduto = produtodto.NomeProduto;
                pedidofornecedor.NomeFornecedor = fornecedoresdto.Nm_empresa;
                pedidofornecedor.FkProdutoFornecedor = produtodto.IDprodutoFornecedor;

                FornecedorDTO dto5 = lista1[0];
                preco = dto5.preco_unitario;
                decimal quantidade = 0;

                quantidade = Convert.ToDecimal((txtquantidade.Text));
                total = preco * quantidade;
                pedidofornecedor.Quantidade = int.Parse(txtquantidade.Text);
                pedidofornecedor.PrecoTotal = total;
                int fk = UserSession.UsuarioLogado.Id_funcionario;
                pedidofornecedor.FKFuncionario = fk;
                pedidofornecedor.DataCompra = DateTime.Now;

                produtosfornecedor.Add(pedidofornecedor);
            }
            else if (txtquantidade.Text == null)
            {
                MessageBox.Show("Por favor preencha o campo quantidade.", "Back's", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

        }


        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                ColocarDadosNagrid();

            }


            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void CarregarComboFornecedor()
        {

           FornecedorBusiness business2 = new FornecedorBusiness();
            lista1 = business2.Listar();
            cboFornecedor.DisplayMember = nameof(FornecedorDTO.Nm_empresa);
            cboFornecedor.DataSource = lista1;


        }

        private void Grid()
        {
            dtg_consultarproduto.AutoGenerateColumns = false;
            dtg_consultarproduto.DataSource = produtosfornecedor;


        }

        private void LimparCampos()
        {
            cboFornecedor.Text = string.Empty;
            cboProduto.Text = string.Empty;
            txtquantidade.Text = string.Empty;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            LimparCampos();
            MessageBox.Show("Pedido Cancelado.");
        }

        private void cboProduto_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
          
        }

        private void button2_Click(object sender, EventArgs e)
        {
          try
            {
                if (txtquantidade.Text == string.Empty)
                {
                    MessageBox.Show("Por favor preencha todos os campos.", "House of Taste", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    PedidoFornecedorBusiness fornecedorBusiness = new PedidoFornecedorBusiness();
                    fornecedorBusiness.Salvar(produtosfornecedor.ToList());
                    MessageBox.Show("Dados salvos com sucesso.", "House of Taste", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

            }
            catch (Exception)
            {

            }

        }
    }
}
