﻿using House_of_Taste.ClassesBDFornecedor;
using House_of_Taste.ClassesBDFuncionario;
using House_of_Taste.Telas.Menu;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace House_of_Taste.Telas.Fornecedor
{
    public partial class CadastroFornecedores : Form
    {
        public CadastroFornecedores()
        {
            InitializeComponent();
        }

        private void CadastroFornecedores_Load(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void label1_Click(object sender, EventArgs e)
        {
            TelaInicial menu = new TelaInicial();
            menu.Show();
            this.Close();
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            Menu.TelaInicial menu = new Menu.TelaInicial();
            menu.Show();
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            ConsultarFornecedores consultarfornecedores = new ConsultarFornecedores();
            consultarfornecedores.Show();
            this.Close();
        }
        
        private void Valores() {
            FornecedorDTO dto = new FornecedorDTO();

            dto.Nm_empresa = txtnome.Text;
           dto.CNPJ = txtcnpj.Text;
            dto.rua= txtrua.Text;
            dto.bairro= txtbairro.Text;
            dto.numero = txtnumero.Text;
            dto.email= txtemail.Text;
            dto.uf = txtuf.Text;
            dto.telefone= txtTelefone.Text;
            dto.cep= txtCep.Text;
            dto.complemento= txtComplemento.Text;
            dto.cidade= txtCidade.Text;
            dto.observacoes= txtObs.Text;
            dto.preco_unitario= decimal.Parse(txtPrecoUnitario.Text);
            int pk = UserSession.UsuarioLogado.Id_funcionario;
            dto.fk_id_cadastro_funcionario_para_fo = pk;
            FornecedorBusiness business = new FornecedorBusiness();
            business.Salvar(dto);

            MessageBox.Show("Salvo com sucesso", "House of Taste",MessageBoxButtons.OK,MessageBoxIcon.Information);

        }
       

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Valores();
            MessageBox.Show("Fornecedor Cadastrado com Sucesso.");
        }

        private void txtObs_TextChanged(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            VincularProduto produtoFornecedor = new VincularProduto();
            produtoFornecedor.Show();
        }
    }
}
