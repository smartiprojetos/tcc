﻿using Correios;
using House_of_Taste.Telas.Menu;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace House_of_Taste.Telas.Fornecedor
{
    public partial class FiltrarEndereco : Form
    {
        public FiltrarEndereco()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {
            TelaInicial menu = new TelaInicial();
            menu.Show();
            this.Close();
        }

        private void FiltrarEndereco_Load(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            Menu.TelaInicial menu = new Menu.TelaInicial();
            menu.Show();
            this.Close();
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            ConsultarFornecedores consultarfornecedores = new ConsultarFornecedores();
            consultarfornecedores.Show();
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            
        }

        private void pictureBox5_Click(object sender, EventArgs e)
        {
            string cep = txtcep.Text;
            CorreiosApi correios = new CorreiosApi();
            var dados = correios.consultaCEP(cep);
            string bairro = dados.bairro;
            string cidade = dados.cidade;
            string complemento = dados.complemento;
            string end = dados.end;
            cbouf.SelectedItem = dados.uf;

            txtcomplemento.Text = complemento;
            txtend.Text = end + " , " + bairro + " , " + cidade + " ";

        }

        private void txtcep_TextChanged(object sender, EventArgs e)
        {
            string cep = txtcep.Text;

            Regex rg = new Regex(@"^\d{5}-\d{3}$");

            bool validou = rg.IsMatch(txtcep.Text);
            if (validou == true)
            {
                txtcep.BackColor = Color.LawnGreen;
            }
            else
            {
                txtcep.BackColor = Color.IndianRed;
            }
        }
    }
}
