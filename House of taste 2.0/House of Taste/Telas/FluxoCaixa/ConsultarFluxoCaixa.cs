﻿using House_of_Taste.ClassesBDFluxoDeCaixa;
using House_of_Taste.Telas.Menu;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace House_of_Taste.Telas.FluxoCaixa
{
    public partial class ConsultarFluxoCaixa : Form
    {
        public ConsultarFluxoCaixa()
        {
            InitializeComponent();
        }

        private void label5_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void label1_Click(object sender, EventArgs e)
        {
            TelaInicial menu = new TelaInicial();
            menu.Show();
            this.Close();
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            Menu.TelaInicial menu = new Menu.TelaInicial();
            menu.Show();
            this.Close();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            FluxoCaixa fluxocaixa = new FluxoCaixa();
            fluxocaixa.Show();
            this.Close();
        }

        private void dtg_consultarproduto_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
        private void CarregarDados()
        {
            FluxoCaixaBusiness business = new FluxoCaixaBusiness();
            dtg_consultarproduto.AutoGenerateColumns = false;
            List<FluxoCaixaDTO> lista = business.Listar();
            dtg_consultarproduto.DataSource = lista;

        }

        private void button1_Click(object sender, EventArgs e)
        {
            CarregarDados();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            FluxoCaixaBusiness produtobusiness = new FluxoCaixaBusiness();
            FluxoCaixaDTO dto = dtg_consultarproduto.CurrentRow.DataBoundItem as FluxoCaixaDTO;
            produtobusiness.Alterar(dto);
            CarregarDados();
            MessageBox.Show("Os dados selecionados foram alterados.");
        }

        private void button3_Click(object sender, EventArgs e)
        {
            FluxoCaixaBusiness produtobusiness = new FluxoCaixaBusiness();
            FluxoCaixaDTO dto = dtg_consultarproduto.CurrentRow.DataBoundItem as FluxoCaixaDTO;
            produtobusiness.Remover(dto.Id_fluxo_de_caixa);
            CarregarDados();
            MessageBox.Show("Os dados selecionados foram deletados.");
        }
    }
}
