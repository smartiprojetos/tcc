﻿using House_of_Taste.ClassesBDFluxoDeCaixa;
using House_of_Taste.ClassesBDFuncionario;
using House_of_Taste.Telas.Menu;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace House_of_Taste.Telas.FluxoCaixa
{
    public partial class FluxoCaixa : Form
    {
        public FluxoCaixa()
        {
            InitializeComponent();
        }

        private void label5_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void label1_Click(object sender, EventArgs e)
        {
            TelaInicial menu = new TelaInicial();
            menu.Show();
            this.Close();
        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {
            Menu.TelaInicial menu = new Menu.TelaInicial();
            menu.Show();
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ConsultarFluxoCaixa consultarfluxocaixa = new ConsultarFluxoCaixa();
            consultarfluxocaixa.Show();
            this.Close();
        }
        private void salvar()
        {
            int fk = UserSession.UsuarioLogado.Id_funcionario;
            FluxoCaixaDTO dto = new FluxoCaixaDTO();
            dto.numerocaixa = Convert.ToInt32(nudnumero.Text);
            dto.qnt_venda_avista = Convert.ToInt32(nudquantidadev.Text);
            dto.qnt_venda_cartao = Convert.ToInt32(nudquantidadec.Text);
            dto.vl_total_recebido = Convert.ToDecimal(vlrecebido.Text);
            dto.data = dateTimePicker1.Value;
            dto.FKIDfuncionarios = fk;
           
            FluxoCaixaBusiness salva = new FluxoCaixaBusiness();
            salva.Salvar(dto);
        }
        private void button2_Click(object sender, EventArgs e)
        {
          
                salvar();
                MessageBox.Show("Dados salvos com sucesso.", "Salvo", MessageBoxButtons.OK, MessageBoxIcon.Information);
           
        
        }

        private void FluxoCaixa_Load(object sender, EventArgs e)
        {

        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            nudnumero.Text = string.Empty;
            nudquantidadev.Text = string.Empty;
            nudquantidadec.Text = string.Empty;
            vlrecebido.Text = string.Empty;
            dateTimePicker1.Value = DateTime.Now;
        }
    }
}
