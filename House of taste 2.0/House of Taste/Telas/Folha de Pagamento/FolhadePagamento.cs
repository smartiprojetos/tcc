﻿using House_of_Taste.BancodeDados.FolhaPgmt;
using House_of_Taste.ClassesBDFuncionario;
using House_of_Taste.Telas.Menu;
using House_of_Taste.Utilitarios;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace House_of_Taste.Telas.Folha_de_Pagamento
{
    public partial class FolhadePagamento : Form
    {
        public FolhadePagamento()
        {
            InitializeComponent();
        }
        decimal liquido;
        int idfuncionario;
        private void rdbNaoS_CheckedChanged(object sender, EventArgs e)
        {
           
          
        }

        private void label1_Click(object sender, EventArgs e)
        {
            TelaInicial menu = new TelaInicial();
            menu.Show();
            this.Close();
        }

        private void label5_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {
            Menu.TelaInicial menu = new Menu.TelaInicial();
            menu.Show();
            this.Close();
        }

        private void label14_Click(object sender, EventArgs e)
        {

        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void label15_Click(object sender, EventArgs e)
        {

        }

        private void txtsalariobruto_TextChanged(object sender, EventArgs e)
        {

        }

        private void label13_Click(object sender, EventArgs e)
        {

        }

        private void BusCarCPF()
        {
            //Buscando os dados de funcionario para jogar na tela
            FuncionarioBusiness funcionarios = new FuncionarioBusiness();
            List<FuncionarioDTO> lista = funcionarios.ConsultarporNome(txtnomefuncionario.Text);
            FuncionarioDTO dto = lista[0];
            txtCargo.Text = dto.cargo;
            txtcpf.Text = dto.cpf;
            idfuncionario = dto.Id_funcionario;
        }

        private void GerarFolha()
        {
            try
            {
                FolhaPgmtDTO dto = new FolhaPgmtDTO();


                if (txtcpf.BackColor == Color.IndianRed)
                {
                    MessageBox.Show("É necessario verificar o CPF digitado");
                }
                else 
                {

                }
                dto.ds_DiasTrabalhados = txtdiastrabalhados.Text ==String.Empty ? 0 : int.Parse(txtdiastrabalhados.Text);
                dto.ds_HoraE100 = txtHorasE100.Text == string.Empty ? 0 : Convert.ToInt32(txtHorasE100.Text);
                dto.ds_HoraE50 = txtHorasE50.Text == string.Empty ? 0 : Convert.ToInt32(txtHorasE50.Text);
                dto.ds_Mensagem = txtMensagem.Text;
                dto.dt_Registro = mcdiapagamento.SelectionStart; ;
                dto.fk_FolhaPgmt_Func = idfuncionario;
                dto.vl_SalarioBruto = txtSalarioBruto.Text == string.Empty ? 0 : Convert.ToDecimal(txtSalarioBruto.Text);
                dto.FKEmpresa = 1;
                FolhaPgmtBusiness business = new FolhaPgmtBusiness();

                liquido = business.SalvarFolha(dto);
                lblSalario.Text = liquido.ToString();

                MessageBox.Show("Folha de Pagamento criada com sucesso!", "Back's", MessageBoxButtons.OK, MessageBoxIcon.Information);
                
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Back's", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("cadeia de caracteres"))
                    MessageBox.Show("O salário não está num formato correto!");
                else
                    MessageBox.Show("Ocorreu um erro não identificado: " + ex.Message, "Back's", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void Pesquisar_Click(object sender, EventArgs e)
        {
            BusCarCPF();
        }

        private void rdbSimS_CheckedChanged(object sender, EventArgs e)
        {
           
           
        }




        private void Relatorio()
        {

            string
                  cargo,
                  nome,
                  diastrabalhados,
                  valealimentacao = "",
                  planosaude = "",
                  horastotal,
                  salariototal,
                  conteudo;

            nome = txtcpf.Text;
            cargo = txtCargo.Text;
            diastrabalhados = txtdiastrabalhados.Text;
            salariototal = lblSalario.Text;



            conteudo = "O funcionario "
                        + nome
                        + " que efetuou o trabalho de "
                        + cargo
                        + " trabalhou durante "
                        + diastrabalhados
                        + " por "
                        //+ horastotal
                        + " recebendo "
                        + salariototal;
                        //+ "Esse valor possui os descontos de : "
                        //+ " Vale Alimentação : "
                        //+ valealimentacao;


            PDF pdf = new PDF();
            pdf.pdf(conteudo);


        }

        private void btnFim_Click(object sender, EventArgs e)
        {
            Relatorio();
        }

        private void button1_Click(object sender, EventArgs e)
        {
          
            
                GerarFolha();
            
           
        }

        private void button2_Click(object sender, EventArgs e)
        {

        }

        private void txtcpf_TextChanged(object sender, EventArgs e)
        {
            string cpf = txtcpf.Text;

            Regex rg = new Regex(@"^\d{3}\.\d{3}\.\d{3}-\d{2}$");

            bool validou = rg.IsMatch(txtcpf.Text);
            if (validou == true)
            {
                txtcpf.BackColor = Color.LawnGreen;
            }
            else
            {
                txtcpf.BackColor = Color.IndianRed;
            }
        }

        private void txtdiastrabalhados_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != ','))
            {
                e.Handled = true;
                MessageBox.Show("Este campo aceita somente numero e virgula", "Back's", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            if ((e.KeyChar == ',') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
                MessageBox.Show("este campo aceita somente uma virgula", "Back's", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void txtHorasE50_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != ','))
            {
                e.Handled = true;
                MessageBox.Show("Este campo aceita somente numero e virgula", "Back's", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            if ((e.KeyChar == ',') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
                MessageBox.Show("este campo aceita somente uma virgula", "Back's", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void txtHorasE100_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtHorasE100_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != ','))
            {
                e.Handled = true;
                MessageBox.Show("Este campo aceita somente numero e virgula", "Back's", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            if ((e.KeyChar == ',') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
                MessageBox.Show("este campo aceita somente uma virgula", "Back's", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

        }

        private void txtSalarioBruto_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != ','))
            {
                e.Handled = true;
                MessageBox.Show("Este campo aceita somente numero e virgula", "Back's", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            if ((e.KeyChar == ',') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
                MessageBox.Show("este campo aceita somente uma virgula", "Back's", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
    }
}
