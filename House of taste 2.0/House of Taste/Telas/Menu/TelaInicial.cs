﻿using House_of_Taste.ClassesBDFuncionario;
using House_of_Taste.Telas.Controle_de_Cliente;
using House_of_Taste.Telas.Estoque;
using House_of_Taste.Telas.Folha_de_Pagamento;
using House_of_Taste.Telas.Fornecedor;
using House_of_Taste.Telas.LogoffSystem;
using House_of_Taste.Telas.Produtos;
using House_of_Taste.Telas.Usuário;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace House_of_Taste.Telas.Menu
{
    public partial class TelaInicial : Form
    {
        public TelaInicial()
        {
            InitializeComponent();
            CarregarPermissoes();
            string usuario = UserSession.UsuarioLogado.nomecompleto.ToString();
            string nome = UserSession.UsuarioLogado.Id_funcionario.ToString();
            lblusu.Text = usuario;
            lblid.Text = nome;
        }
        private void CarregarPermissoes()
        {
            if(UserSession.UsuarioLogado.permissao_cadastrar==false)
            {
                cadastrarToolStripMenuItem.Enabled = false;
            }
            if (UserSession.UsuarioLogado.permissao_funcionarios == false)
            {
                funcionárioToolStripMenuItem.Enabled = false;
            }
            if(UserSession.UsuarioLogado.permissao_cadastrar_funcionarios==false)
            {
                cadastrarToolStripMenuItem1.Enabled = false;
            }
            if(UserSession.UsuarioLogado.permissao_consultar_funcionarios==false)
            {
                consultarCadastradosToolStripMenuItem.Enabled = false;
            }
            if(UserSession.UsuarioLogado.permissao_produtos==false)
            {
                produtoToolStripMenuItem.Enabled = false;
            }
            if(UserSession.UsuarioLogado.permissao_cadastrar_novo_produto==false)
            {
                cadastrarNovoToolStripMenuItem.Enabled = false;
            }
            if(UserSession.UsuarioLogado.consultar_produtos==false)
            {
                consultarCadastradosToolStripMenuItem1.Enabled = false;
            }
            if(UserSession.UsuarioLogado.permissao_cardapio==false)
            {
                cardápioToolStripMenuItem.Enabled = false;
            }
            if(UserSession.UsuarioLogado.permissao_cadastro_novo_cardapio==false)
            {
                cadastrarNovoToolStripMenuItem1.Enabled = false;
            }
            if(UserSession.UsuarioLogado.permissao_consultar_cardapio==false)
            {
                consultarCadastradosToolStripMenuItem2.Enabled = false;
            }
            if(UserSession.UsuarioLogado.permissao_fornecedor==false)
            {
                fornecedorToolStripMenuItem.Enabled = false;
            }
            if(UserSession.UsuarioLogado.permissao_novo_fornecedor==false)
            {
                cadastrarNovoToolStripMenuItem2.Enabled = false;
            }
            if(UserSession.UsuarioLogado.permissao_consultar_fornecedor==false)
            {
                consultarCadastradosToolStripMenuItem3.Enabled = false;
            }
            if(UserSession.UsuarioLogado.permissao_estoque==false)
            {
                estoqueToolStripMenuItem.Enabled = false;
            }
            if(UserSession.UsuarioLogado.permissao_atualizar_estoque==false)
            {
                atualizarEstoqueToolStripMenuItem.Enabled = false;
            }
            if(UserSession.UsuarioLogado.permissao_consultar_estoque==false)
            {
                consultarToolStripMenuItem.Enabled = false;
            }
            if (UserSession.UsuarioLogado.permissao_pedido_fornecedor == false)
            {
                pedidoFornecedorToolStripMenuItem.Enabled = false;
            }
            if(UserSession.UsuarioLogado.permissao_cliente==false)
            {
                clienteToolStripMenuItem.Enabled = false;
            }
            if(UserSession.UsuarioLogado.permissao_controle_cliente==false)
            {
                controleDeClienteToolStripMenuItem.Enabled = false;
            }
            if(UserSession.UsuarioLogado.permissao_consultar_cliente==false)
            {
                consultarClientesToolStripMenuItem.Enabled=false;
            }
              
        }
        
        private void pedirNovoItemToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void Menu_Load(object sender, EventArgs e)
        {

        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void label1_Click(object sender, EventArgs e)
        {
            Logoff logoff = new Logoff();
            logoff.Show();
            this.Close();
           
        }

        private void cadastrarToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            CadastroFuncionario funcionario = new CadastroFuncionario();
            funcionario.Show();
            this.Close();

        }

        private void funcionárioToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void folhaDePagamentoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FolhadePagamento folhadepagamento = new FolhadePagamento();
            folhadepagamento.Show();
            this.Close();
        }

        private void clienteToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void estoqueToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void fluxoDeCaixaToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void consultarCadastradosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ConsultarFuncionarios consultarfuncionarios = new ConsultarFuncionarios();
            consultarfuncionarios.Show();
            this.Close();
        }

        private void cadastrarNovoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CadastroProduto cadastroproduto = new CadastroProduto();
            cadastroproduto.Show();
            this.Close();
        }

        private void consultarCadastradosToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            ConsultarProdutos consultarprodutos = new ConsultarProdutos();
            consultarprodutos.Show();
            this.Close();
        }

        private void cadastrarNovoToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            CadastroCardapio cadastrocardapio = new CadastroCardapio();
            cadastrocardapio.Show();
            this.Close();
        }

        private void consultarCadastradosToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            ConsultarCardapio consultarcardapio = new ConsultarCardapio();
            consultarcardapio.Show();
            this.Close();
        }

        private void cadastrarNovoToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            CadastroFornecedores cadastrofornecedores = new CadastroFornecedores();
            cadastrofornecedores.Show();
            this.Close();
        }

        private void consultarCadastradosToolStripMenuItem3_Click(object sender, EventArgs e)
        {
            ConsultarFornecedores consultarfornecedores = new ConsultarFornecedores();
            consultarfornecedores.Show();
            this.Close();
        }

        private void atualizarEstoqueToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CadastrarEstoque estoque = new CadastrarEstoque();
            estoque.Show();
            this.Close();
        }

        private void consultarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ConsultarEstoque consultarestoque = new ConsultarEstoque();
            consultarestoque.Show();
            this.Close();
        }

        private void pedirFornecedorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PedidoFornecedor pedidofornecedor = new PedidoFornecedor();
            pedidofornecedor.Show();
            this.Close();
        }

        private void filtrarEndereçoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FiltrarEndereco filtrarendereco = new FiltrarEndereco();
            filtrarendereco.Show();
            this.Close();
        }

        private void controleDeClienteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ControleCliente controlecliente = new ControleCliente();
            controlecliente.Show();
            this.Close();
        }

        private void consultarClientesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ConsultarClientes consultarclientes = new ConsultarClientes();
            consultarclientes.Show();
            this.Close();
        }

        private void cadastrarToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            FluxoCaixa.FluxoCaixa fluxocaixa = new FluxoCaixa.FluxoCaixa();
            fluxocaixa.Show();
            this.Close();
        }

        private void consultarToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            FluxoCaixa.ConsultarFluxoCaixa consultarfluxocaixa = new FluxoCaixa.ConsultarFluxoCaixa();
            consultarfluxocaixa.Show();
            this.Close();
        }

        private void pedidoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ConsultarPedido consultarpedido = new ConsultarPedido();
            consultarpedido.Show();
            this.Close();
        }

        private void pedidoFornecedorToolStripMenuItem_Click(object sender, EventArgs e)
        {
           
        }

        private void consultarPedidosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ConsultarPedido consultarpedido = new ConsultarPedido();
            consultarpedido.Show();
            this.Close();
        }

        private void vincularProdutoFornecedorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            VincularProduto vincular = new VincularProduto();
            vincular.Show();
            this.Close();
        }
    }
}
