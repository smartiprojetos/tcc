﻿using House_of_Taste.Telas;
using House_of_Taste.Telas.Folha_de_Pagamento;
using House_of_Taste.Telas.Menu;
using House_of_Taste.Telas.TeladeSplash;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace House_of_Taste
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new TelaSplash());
        }
    }
}
