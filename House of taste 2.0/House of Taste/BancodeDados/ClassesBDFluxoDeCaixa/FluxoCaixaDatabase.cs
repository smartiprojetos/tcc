﻿//using House_of_Taste.BancodeDados.ClassesBDFluxoDeCaixa;
using House_of_Taste.BD;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace House_of_Taste.ClassesBDFluxoDeCaixa
{
    class FluxoCaixaDatabase
    {
        public int Salvar(FluxoCaixaDTO dto)
        {
            string script = @"insert into tb_fluxo_de_caixa (qnt_de_venda_avista,qnt_de_venda_cartao,vl_total_recebido,fk_id_funcionario_para_fc, data, numerocaixa)
                           values     (@qnt_de_venda_avista,@qnt_de_venda_cartao,@vl_total_recebido,@fk_id_funcionario_para_fc, @data, @numerocaixa)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("qnt_de_venda_avista", dto.qnt_venda_avista));
            parms.Add(new MySqlParameter("qnt_de_venda_cartao", dto.qnt_venda_cartao));
            parms.Add(new MySqlParameter("vl_total_recebido", dto.vl_total_recebido));
            parms.Add(new MySqlParameter("data", dto.data));
            parms.Add(new MySqlParameter("numerocaixa", dto.numerocaixa));
            parms.Add(new MySqlParameter("fk_id_funcionario_para_fc", dto.FKIDfuncionarios));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }

        public void Alterar(FluxoCaixaDTO dto)
        {
            string script = @"update tb_fluxo_de_caixa set   qnt_de_venda_avista=@qnt_de_venda_avista,
                                                            qnt_de_venda_cartao=@qnt_de_venda_cartao,
                                                            vl_total_recebido=@vl_total_recebido,
                                                           data=@data,
numerocaixa=@numerocaixa,
                                                            fk_id_funcionario_para_fc=@fk_id_funcionario_para_fc
                                            where           id_fluxo_de_caixa = @id_fluxo_de_caixa";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_fluxo_de_caixa", dto.Id_fluxo_de_caixa));
            parms.Add(new MySqlParameter("qnt_de_venda_avista", dto.qnt_venda_avista));
            parms.Add(new MySqlParameter("qnt_de_venda_cartao", dto.qnt_venda_cartao));
            parms.Add(new MySqlParameter("vl_total_recebido", dto.vl_total_recebido));
            parms.Add(new MySqlParameter("fk_id_funcionario_para_fc", dto.FKIDfuncionarios));
            parms.Add(new MySqlParameter("data", dto.data));
            parms.Add(new MySqlParameter("numerocaixa", dto.numerocaixa));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }

        public void Deletar(int Id)
        {
            string script = @"delete from tb_fluxo_de_caixa where id_fluxo_de_caixa = @id_fluxo_de_caixa";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_fluxo_de_caixa", Id));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }


        public List<FluxoCaixaDTO> Listar()
        {
            string script = @"select * from tb_fluxo_de_caixa";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);
            List<FluxoCaixaDTO> lista = new List<FluxoCaixaDTO>();
            while (reader.Read())
            {
                FluxoCaixaDTO dto = new FluxoCaixaDTO();
                dto.Id_fluxo_de_caixa = reader.GetInt32("id_fluxo_de_caixa");
                dto.qnt_venda_avista = reader.GetInt32("qnt_de_venda_avista");
                dto.qnt_venda_cartao = reader.GetInt32("qnt_de_venda_cartao");
                dto.vl_total_recebido = reader.GetDecimal("vl_total_recebido");
                dto.data = reader.GetDateTime("data");
                dto.FKIDfuncionarios = reader.GetInt32("fk_id_funcionario_para_fc");
                dto.numerocaixa = reader.GetInt32("numerocaixa");
                lista.Add(dto);
            }
            reader.Close();
            return lista;
        }



        public List<FluxoCaixaDTO> ConsultarporID(int ID)
        {
            string script = @"select * from tb_fluxo_de_caixa where id_fluxo_de_caixa like @id_fluxo_de_caixa";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_fluxo_de_caixa", ID + "%"));
            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);
            List<FluxoCaixaDTO> lista = new List<FluxoCaixaDTO>();
            while (reader.Read())
            {
                FluxoCaixaDTO dto = new FluxoCaixaDTO();
                dto.Id_fluxo_de_caixa = reader.GetInt32("id_fluxo_de_caixa");
                dto.qnt_venda_avista = reader.GetInt32("qnt_de_venda_avista");
                dto.qnt_venda_cartao = reader.GetInt32("qnt_de_venda_cartao");
                dto.vl_total_recebido = reader.GetDecimal("vl_total_recebido");
                dto.data = reader.GetDateTime("data");
                dto.FKIDfuncionarios = reader.GetInt32("fk_id_funcionario_para_fc");
                dto.numerocaixa = reader.GetInt32("numerocaixa");

                lista.Add(dto);
            }
            reader.Close();
            return lista;
        }
    }
}






