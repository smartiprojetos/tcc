﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace House_of_Taste.ClassesBDFluxoDeCaixa
{
    public class FluxoCaixaDTO
    {
      public int Id_fluxo_de_caixa { get; set; }
        public int qnt_venda_avista { get; set; }
        public int qnt_venda_cartao { get; set; }
        public decimal vl_total_recebido { get; set; }
       public int numerocaixa { get; set; }
        public int FKIDfuncionarios { get; set; }
        public DateTime data { get; set; }

    }
}
