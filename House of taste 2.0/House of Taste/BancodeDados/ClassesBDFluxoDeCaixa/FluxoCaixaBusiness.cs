﻿//using House_of_Taste.BancodeDados.ClassesBDFluxoDeCaixa;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace House_of_Taste.ClassesBDFluxoDeCaixa
{
    class FluxoCaixaBusiness
    {
        public int Salvar(FluxoCaixaDTO dto)
        {
            FluxoCaixaDatabase db = new FluxoCaixaDatabase();
            return db.Salvar(dto);
        }

        public void Alterar(FluxoCaixaDTO dto)
        {
            FluxoCaixaDatabase db = new FluxoCaixaDatabase();
            db.Alterar(dto);
        }

        public void Remover(int ID)
        {
            FluxoCaixaDatabase db = new FluxoCaixaDatabase();
            db.Deletar(ID);
        }

        public List<FluxoCaixaDTO> Listar()
        {
            FluxoCaixaDatabase db = new FluxoCaixaDatabase();
            return db.Listar();
        }

        public List<FluxoCaixaDTO> ConsultarporID(int id)
        {
            FluxoCaixaDatabase db = new FluxoCaixaDatabase();
            return db.ConsultarporID(id);
        }


 
    }
}
