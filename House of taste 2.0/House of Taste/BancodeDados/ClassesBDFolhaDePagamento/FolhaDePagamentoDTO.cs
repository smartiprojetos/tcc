﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace House_of_Taste.BancodeDados.ClassesBDFolhaDePagamento
{
    public class FolhaDePagamentoDTO
    {
        public int IdFolhadePagamento { get; set; }
        public int DiasTrabalhados { get; set; }
        public decimal SalarioHora { get; set; }
        public DateTime HorasTrabalhadas { get; set; }
        public decimal PlanodeSaude { get; set; }
        public decimal ValeAlimentacao { get; set; }
        public decimal SalarioBruto { get; set; }
        public decimal SalarioMes { get; set; }
        public decimal ValeTrasnporte { get; set; }
        public decimal Adiantamento { get; set; }
        public decimal INSS { get; set; }
        public decimal Descontos { get; set; }
        public decimal SalarioBase { get; set; }
        public decimal CalculoINSS { get; set; }
        public decimal CalculobaseFGTS { get; set; }
        public decimal FGTSmes { get; set; }
        public decimal CalculoBaseIRRF { get; set; }
        public decimal FaixaIRRF { get; set; }
        public decimal TotalVencimentos { get; set; }
        public decimal Referencia { get; set; }
        public string Mensagem { get; set; }
        public DateTime DiaPagamento { get; set; }
        public int FKidfuncionario { get; set; }
        public int FKIdempresa { get; set; }
    }
}
