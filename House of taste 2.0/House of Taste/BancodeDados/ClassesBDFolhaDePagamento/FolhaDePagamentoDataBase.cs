﻿using House_of_Taste.BD;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace House_of_Taste.BancodeDados.ClassesBDFolhaDePagamento
{
    public class FolhaDePagamentoDataBase
    {
        public int Salvar(FolhaDePagamentoDTO dto)
        {
            string script = @"insert into tb_folha_de_pagamento    (dias_trabalhados,
                                                                    sl_hora,
                                                                    hr_horas_trabalhadas,
                                                                    pl_plano_de_saude,
                                                                    vl_vale_alimentacao,
                                                                    sl_salario_bruto,
                                                                    sl_salario_mes,
                                                                    vl_vale_transporte,
                                                                    ad_adiantamento,
                                                                    vl_inss,
                                                                    ds_descontos,
                                                                    sl_salario_base,
                                                                    cl_calculo_inss,
                                                                    cl_calculo_base_fgts,
                                                                    ft_fgts_mes,
                                                                    bs_base_calc_irrf,
                                                                    fi_faixa_irrf,
                                                                    tl_total_vencimentos,
                                                                    rf_referencia,
                                                                    ms_mensagem,
                                                                    fk_id_funcionario_fp,
                                                                    fk_id_empresa_fp,
                                                                    dia_pagamento)
                                                               values
                                                                   (@dias_trabalhados,
                                                                    @sl_hora,
                                                                    @hr_horas_trabalhadas,
                                                                    @pl_plano_de_saude,
                                                                    @vl_vale_alimentacao,
                                                                    @sl_salario_bruto,
                                                                    @sl_salario_mes,
                                                                    @vl_vale_transporte,
                                                                    @ad_adiantamento,
                                                                    @vl_inss,
                                                                    @ds_descontos,
                                                                    @sl_salario_base,
                                                                    @cl_calculo_inss,
                                                                    @cl_calculo_base_fgts,
                                                                    @ft_fgts_mes,
                                                                    @bs_base_calc_irrf,
                                                                    @fi_faixa_irrf,
                                                                    @tl_total_vencimentos,
                                                                    @rf_referencia,
                                                                    @ms_mensagem,
                                                                    @fk_id_funcionario_fp,
                                                                    @fk_id_empresa_fp,
                                                                    @dia_pagamento)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("dias_trabalhados", dto.DiasTrabalhados));
            parms.Add(new MySqlParameter("sl_hora", dto.SalarioHora));
            parms.Add(new MySqlParameter("hr_horas_trabalhadas", dto.HorasTrabalhadas));
            parms.Add(new MySqlParameter("pl_plano_de_saude", dto.PlanodeSaude));
            parms.Add(new MySqlParameter("vl_vale_alimentacao", dto.ValeAlimentacao));
            parms.Add(new MySqlParameter("sl_salario_bruto", dto.SalarioBruto));
            parms.Add(new MySqlParameter("sl_salario_mes", dto.SalarioMes));
            parms.Add(new MySqlParameter("vl_vale_transporte", dto.ValeTrasnporte));
            parms.Add(new MySqlParameter("ad_adiantamento", dto.Adiantamento));
            parms.Add(new MySqlParameter("vl_inss", dto.INSS));
            parms.Add(new MySqlParameter("ds_descontos", dto.Descontos));
            parms.Add(new MySqlParameter("sl_salario_base", dto.SalarioBase));
            parms.Add(new MySqlParameter("cl_calculo_inss", dto.CalculoINSS));
            parms.Add(new MySqlParameter("cl_calculo_base_fgts", dto.CalculobaseFGTS));
            parms.Add(new MySqlParameter("ft_fgts_mes", dto.FGTSmes));
            parms.Add(new MySqlParameter("bs_base_calc_irrf", dto.CalculoBaseIRRF));
            parms.Add(new MySqlParameter("fi_faixa_irrf", dto.FaixaIRRF));
            parms.Add(new MySqlParameter("tl_total_vencimentos", dto.TotalVencimentos));
            parms.Add(new MySqlParameter("rf_referencia", dto.Referencia));
            parms.Add(new MySqlParameter("ms_mensagem", dto.Mensagem));
            parms.Add(new MySqlParameter("fk_id_funcionario_fp", dto.FKidfuncionario));
            parms.Add(new MySqlParameter("fk_id_empresa_fp", dto.FKIdempresa));
            parms.Add(new MySqlParameter("dia_pagamento", dto.DiaPagamento));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }

        public void ALterar(FolhaDePagamentoDTO dto)
        {
            string script = @"update tb_folha_de_pagamento set dias_trabalhados=@dias_trabalhados,
                                                                        sl_hora=@sl_hora,
                                                                        hr_horas_trabalhadas=@hr_horas_trabalhadas,
                                                                        pl_plano_de_saude=@pl_plano_de_saude,
                                                                        vl_vale_alimentacao=@vl_vale_alimentacao,
                                                                        sl_salario_bruto=@sl_salario_bruto,
                                                                        sl_salario_mes=@sl_salario_mes,
                                                                        vl_vale_transporte=@vl_vale_transporte,
                                                                        ad_adiantamento=@ad_adiantamento,
                                                                        vl_inss=@vl_inss,
                                                                        ds_descontos=@ds_descontos,
                                                                        sl_salario_base=@sl_salario_base,
                                                                        cl_calculo_inss=@cl_calculo_inss,
                                                                        cl_calculo_base_fgts=@cl_calculo_base_fgts,
                                                                        ft_fgts_mes=@ft_fgts_mes,
                                                                        bs_base_calc_irrf=@bs_base_calc_irrf,
                                                                        fi_faixa_irrf=@fi_faixa_irrf,
                                                                        tl_total_vencimentos=@tl_total_vencimentos,
                                                                        rf_referencia=@rf_referencia,
                                                                        ms_mensagem=@ms_mensagem,
                                                                        fk_id_funcionario_fp=@fk_id_funcionario_fp,
                                                                        fk_id_empresa_fp=@fk_id_empresa_fp,
                                                                        dia_pagamento=@dia_pagamento
                                                                 where  id_folha_de_pagamento=@id_folha_de_pagamento";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_folha_de_pagamento", dto.IdFolhadePagamento));
            parms.Add(new MySqlParameter("dias_trabalhados", dto.DiasTrabalhados));
            parms.Add(new MySqlParameter("sl_hora", dto.SalarioHora));
            parms.Add(new MySqlParameter("hr_horas_trabalhadas", dto.HorasTrabalhadas));
            parms.Add(new MySqlParameter("pl_plano_de_saude", dto.PlanodeSaude));
            parms.Add(new MySqlParameter("vl_vale_alimentacao", dto.ValeAlimentacao));
            parms.Add(new MySqlParameter("sl_salario_bruto", dto.SalarioBruto));
            parms.Add(new MySqlParameter("sl_salario_mes", dto.SalarioMes));
            parms.Add(new MySqlParameter("vl_vale_transporte", dto.ValeTrasnporte));
            parms.Add(new MySqlParameter("ad_adiantamento", dto.Adiantamento));
            parms.Add(new MySqlParameter("vl_inss", dto.INSS));
            parms.Add(new MySqlParameter("ds_descontos", dto.Descontos));
            parms.Add(new MySqlParameter("sl_salario_base", dto.SalarioBase));
            parms.Add(new MySqlParameter("cl_calculo_inss", dto.CalculoINSS));
            parms.Add(new MySqlParameter("cl_calculo_base_fgts", dto.CalculobaseFGTS));
            parms.Add(new MySqlParameter("ft_fgts_mes", dto.FGTSmes));
            parms.Add(new MySqlParameter("bs_base_calc_irrf", dto.CalculoBaseIRRF));
            parms.Add(new MySqlParameter("fi_faixa_irrf", dto.FaixaIRRF));
            parms.Add(new MySqlParameter("tl_total_vencimentos", dto.TotalVencimentos));
            parms.Add(new MySqlParameter("rf_referencia", dto.Referencia));
            parms.Add(new MySqlParameter("ms_mensagem", dto.Mensagem));
            parms.Add(new MySqlParameter("fk_id_funcionario_fp", dto.FKidfuncionario));
            parms.Add(new MySqlParameter("fk_id_empresa_fp", dto.FKIdempresa));
            parms.Add(new MySqlParameter("dia_pagamento", dto.DiaPagamento));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);



        }

        public void Remover(int Id)
        {
            string script =
            @"DELETE FROM tb_folha_de_pagamento WHERE id_folha_de_pagamento = @id_folha_de_pagamento";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_folha_de_pagamento", Id));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);

        }




        public List<FolhaDePagamentoDTO> Listar()
        {
            string script = @"select * from tb_folha_de_pagamento";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);
            List<FolhaDePagamentoDTO> lista = new List<FolhaDePagamentoDTO>();
            while (reader.Read())
            {
                FolhaDePagamentoDTO dto = new FolhaDePagamentoDTO();
                dto.IdFolhadePagamento = reader.GetInt32("id_folha_de_pagamento");
                dto.DiasTrabalhados = reader.GetInt32("dias_trabalhados");
                dto.SalarioHora = reader.GetDecimal("sl_hora");
                dto.HorasTrabalhadas = reader.GetDateTime("hr_horas_trabalhadas");
                dto.PlanodeSaude = reader.GetDecimal("pl_plano_de_saude");
                dto.ValeAlimentacao = reader.GetDecimal("vl_vale_alimentacao");
                dto.SalarioBruto = reader.GetDecimal("sl_salario_bruto");
                dto.SalarioMes = reader.GetDecimal("sl_salario_mes");
                dto.ValeTrasnporte = reader.GetDecimal("vl_vale_transporte");
                dto.Adiantamento = reader.GetDecimal("ad_adiantamento");
                dto.INSS = reader.GetDecimal("vl_inss");
                dto.Descontos = reader.GetDecimal("ds_descontos");
                dto.SalarioBase = reader.GetDecimal("sl_salario_base");
                dto.CalculoINSS = reader.GetDecimal("cl_calculo_inss");
                dto.CalculobaseFGTS = reader.GetDecimal("cl_calculo_base_fgts");
                dto.FGTSmes = reader.GetDecimal("ft_fgts_mes");
                dto.CalculoBaseIRRF = reader.GetDecimal("bs_base_calc_irrf");
                dto.FaixaIRRF = reader.GetDecimal("fi_faixa_irrf");
                dto.TotalVencimentos = reader.GetDecimal("tl_total_vencimentos");
                dto.Referencia = reader.GetDecimal("rf_referencia");
                dto.Mensagem = reader.GetString("ms_mensagem");
                dto.DiaPagamento = reader.GetDateTime("dia_pagamento");
                dto.FKidfuncionario = reader.GetInt32("fk_id_funcionario_fp");
                dto.FKIdempresa = reader.GetInt32("fk_id_empresa_fp");


            }
            reader.Close();
            return lista;

        }

        public List<FolhaDePagamentoDTO> Buscar(int id)
        {
            string script = @"select * from tb_folha_de_pagamento where id_folha_de_pagamento like @id_folha_de_pagamento";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_cadastro_funcionario", id + "%"));
            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);
            List<FolhaDePagamentoDTO> lista = new List<FolhaDePagamentoDTO>();
            while (reader.Read())
            {
                FolhaDePagamentoDTO dto = new FolhaDePagamentoDTO(); 
                dto.IdFolhadePagamento = reader.GetInt32("id_folha_de_pagamento");
                dto.DiasTrabalhados = reader.GetInt32("dias_trabalhados");
                dto.SalarioHora = reader.GetDecimal("sl_hora");
                dto.HorasTrabalhadas = reader.GetDateTime("hr_horas_trabalhadas");
                dto.PlanodeSaude = reader.GetDecimal("pl_plano_de_saude");
                dto.ValeAlimentacao = reader.GetDecimal("vl_vale_alimentacao");
                dto.SalarioBruto = reader.GetDecimal("sl_salario_bruto");
                dto.SalarioMes = reader.GetDecimal("sl_salario_mes");
                dto.ValeTrasnporte = reader.GetDecimal("vl_vale_transporte");
                dto.Adiantamento = reader.GetDecimal("ad_adiantamento");
                dto.INSS = reader.GetDecimal("vl_inss");
                dto.Descontos = reader.GetDecimal("ds_descontos");
                dto.SalarioBase = reader.GetDecimal("sl_salario_base");
                dto.CalculoINSS = reader.GetDecimal("cl_calculo_inss");
                dto.CalculobaseFGTS = reader.GetDecimal("cl_calculo_base_fgts");
                dto.FGTSmes = reader.GetDecimal("ft_fgts_mes");
                dto.CalculoBaseIRRF = reader.GetDecimal("bs_base_calc_irrf");
                dto.FaixaIRRF = reader.GetDecimal("fi_faixa_irrf");
                dto.TotalVencimentos = reader.GetDecimal("tl_total_vencimentos");
                dto.Referencia = reader.GetDecimal("rf_referencia");
                dto.Mensagem = reader.GetString("ms_mensagem");
                dto.DiaPagamento = reader.GetDateTime("dia_pagamento");
                dto.FKidfuncionario = reader.GetInt32("fk_id_empresa_fp");
                dto.FKIdempresa = reader.GetInt32("fk_id_funcionario_fp");

                lista.Add(dto);



            }
            reader.Close();
            return lista;
        }



    }
}
