﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace House_of_Taste.BancodeDados.ClassesBDFolhaDePagamento
{
    public class FolhaDePagamentoBusiness
    {
        public int Salvar(FolhaDePagamentoDTO dto)
        {
            FolhaDePagamentoDataBase db = new FolhaDePagamentoDataBase();

            return db.Salvar(dto);
        }


        public void Alterar(FolhaDePagamentoDTO dto)
        {
            FolhaDePagamentoDataBase db = new FolhaDePagamentoDataBase();
            db.ALterar(dto);
        }

        public void Remover(int id)
        {
            FolhaDePagamentoDataBase db = new FolhaDePagamentoDataBase();
            db.Remover(id);
        }
        public List<FolhaDePagamentoDTO> Listar()
        {
            FolhaDePagamentoDataBase db = new FolhaDePagamentoDataBase();
            return db.Listar();
        }

        public List<FolhaDePagamentoDTO> Buscar(int id)
        {
            FolhaDePagamentoDataBase db = new FolhaDePagamentoDataBase();
            return db.Buscar(id);

        }
    }
}
