﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace House_of_Taste.ClassesBDProduto
{
    public class ProdutoDTO
    {
        //estava nomeado como ConsultarPedidoView caso venha ter um erro no futuro
        public int Id_cadastro_produto { get; set; }
        public string nome_produto { get; set; }
        public DateTime validade { get; set; }
        public decimal preco { get; set; }
        public int fk_funcinario { get; set; }
    }
}
