﻿using House_of_Taste.BD;
using House_of_Taste.ClassesBDEstoque;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace House_of_Taste.ClassesBDProduto
{
    class ProdutoDatabase
    {
        public int Salvar(ProdutoDTO dto)
        {
            string script = @"INSERT INTO tb_cadastro_produtos (nm_produto,dt_validade,pr_preco,fk_id_cadastro_funcionario)
                                                 values     (@nm_produto,@dt_validade,@pr_preco,@fk_id_cadastro_funcionario)";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_produto", dto.nome_produto));
            parms.Add(new MySqlParameter("dt_validade", dto.validade));
            parms.Add(new MySqlParameter("pr_preco", dto.preco));
            parms.Add(new MySqlParameter("fk_id_cadastro_funcionario", dto.fk_funcinario));

            Database db = new Database();
            int pk = db.ExecuteInsertScriptWithPk(script, parms);
            return pk;
        }

        public void Alterar(ProdutoDTO dto)
        {
            string script = @"update tb_cadastro_produtos set 
                                                         nm_produto=@nm_produto,
                                                         dt_validade=@dt_validade,
                                                         pr_preco=@pr_preco,
                                                         fk_id_cadastro_funcionario=@fk_id_cadastro_funcionario 
                                               where        id_cadastro_produtos=@id_cadastro_produtos";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_cadastro_produtos", dto.Id_cadastro_produto));
            parms.Add(new MySqlParameter("nm_produto", dto.nome_produto));
            parms.Add(new MySqlParameter("dt_validade", dto.validade));
            parms.Add(new MySqlParameter("pr_preco", dto.preco));
            parms.Add(new MySqlParameter("fk_id_cadastro_funcionario", dto.fk_funcinario));
            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }

        public List<ProdutoDTO> Listar()
        {
            string script = @"select * from tb_cadastro_produtos";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);
            List<ProdutoDTO> lista = new List<ProdutoDTO>();
            while (reader.Read())
            {
                ProdutoDTO dados = new ProdutoDTO();
                dados.Id_cadastro_produto = reader.GetInt32("id_cadastro_produtos");
                dados.nome_produto = reader.GetString("nm_produto");
                dados.validade = reader.GetDateTime("dt_validade");
                dados.preco= reader.GetDecimal("pr_preco");
                dados.fk_funcinario = reader.GetInt32("fk_id_cadastro_funcionario");
                lista.Add(dados);
            }
            reader.Close();
            return lista;
        }

        public void Remover(int id)
        {
            string script = @"delete from tb_cadastro_produtos where id_cadastro_produtos=@id_cadastro_produtos";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_cadastro_produtos", id));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }

        public List<ProdutoDTO> Consultar(string Produto)
        {
            string script = @"select *from tb_cadastro_produtos where nm_produto like @nm_produto";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_produto", Produto + "%"));
            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);
            List<ProdutoDTO> lista = new List<ProdutoDTO>();
            ProdutoDTO dto = null;
            while (reader.Read())
            {
                dto = new ProdutoDTO();
                dto.Id_cadastro_produto = reader.GetInt32("id_cadastro_produtos");
                dto.nome_produto = reader.GetString("nm_produto");
                dto.validade = reader.GetDateTime("dt_validade");
                dto.preco = reader.GetDecimal("pr_preco");
                dto.fk_funcinario = reader.GetInt32("fk_id_cadastro_funcionario");
                lista.Add(dto);
            }
            reader.Close();
            return lista;
        }

        public List<ProdutoDTO> Buscar(int id)
        {
            string script = @"select * from tb_cadastro_produtos where id_cadastro_produtos like @id_cadastro_produtos";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_cadastro_produtos", id + "%"));
            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);
            List<ProdutoDTO> lista = new List<ProdutoDTO>();
            ProdutoDTO dto = null;
            while (reader.Read())
            {
                dto = new ProdutoDTO();
                dto.Id_cadastro_produto = reader.GetInt32("id_cadastro_produtos");
                dto.nome_produto = reader.GetString("nm_produto");
                dto.validade = reader.GetDateTime("dt_validade");
                dto.fk_funcinario= reader.GetInt32("fk_id_cadastro_funcionario");
                lista.Add(dto);
            }
            reader.Close();
            return lista;
        }

        public List<ProdutoDTO> ConsultarItensdoCardapio(int ID)
        {
            string script = @"select  fk_id_produto_para_es,tb_cadastro_produtos.nm_produto from tb_estoque
                                join tb_produto_no_cardapio
                                on tb_estoque.id_estoque = tb_produto_no_cardapio.fk_id_estoque_para_pc
                                join tb_cadastro_produtos
                                on tb_cadastro_produtos.id_cadastro_produtos = tb_estoque.fk_id_produto_para_es
                                where tb_produto_no_cardapio.fk_id_cardapio_para_pc =" + ID;
            List<MySqlParameter> parms = new List<MySqlParameter>();
            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);
            List<ProdutoDTO> lista = new List<ProdutoDTO>();
            while (reader.Read())
            {
                ProdutoDTO dto = new ProdutoDTO();
                dto.Id_cadastro_produto = reader.GetInt32("fk_id_produto_para_es");
                dto.nome_produto = reader.GetString("nm_produto");

                lista.Add(dto);
            }
            reader.Close();
            return lista;
        }
    }
}

