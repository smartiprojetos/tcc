﻿using House_of_Taste.ClassesBDEstoque;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace House_of_Taste.ClassesBDProduto
{
    class ProdutoBusiness
    {
        public int Salvar(ProdutoDTO dto)
        {
            if (dto.nome_produto == string.Empty)
            {
                throw new ArgumentException("Produto é obrigatório, por favor informar este campo.");
            }

            if (dto.preco == 0)
            {
                throw new ArgumentException("Preço é obrigatório, por favor informe-nos o valor que deseja para este produto.");
            }
            ProdutoDatabase db = new ProdutoDatabase();
            return db.Salvar(dto);
        }

        public List<ProdutoDTO> Listar()
        {
            ProdutoDatabase db = new ProdutoDatabase();
            return db.Listar();
        }

        public void Remover(int id)
        {
            if (id == 0)
            {
                throw new ArgumentException("Id não pode ser nulo.");
            }

            ProdutoDatabase db = new ProdutoDatabase();
            db.Remover(id);
        }

        public List<ProdutoDTO> Consultar(string produto)
        {

            ProdutoDatabase db = new ProdutoDatabase();
            return db.Consultar(produto);
        }

            public void Alterar(ProdutoDTO dto)
            {
                if (dto.nome_produto == string.Empty)
                {
                    throw new ArgumentException("Produto é obrigatório");
                }

                if (dto.preco == 0)
                {
                    throw new ArgumentException("Preço é obrigatório");
                }
                ProdutoDatabase db = new ProdutoDatabase();
                db.Alterar(dto);
            }


        public List<ProdutoDTO> Buscar(int id)
        {
            if (id == 0)
            {
                throw new ArgumentException("Id é obrigatório.");
            }

            ProdutoDatabase db = new ProdutoDatabase();
            return db.Buscar(id);
        }

        public List<ProdutoDTO> ConsultarItensdoCardapio(int ID)
        {
            ProdutoDatabase db = new ProdutoDatabase();
            return db.ConsultarItensdoCardapio(ID);
        }
    }
}
