﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace House_of_Taste.BancodeDados.ProdutoFornecedor
{
    class DTONomedeProdutoeFornecedor
    {
        public int IDprodutoFornecedor { get; set; }
        public int IdProduto { get; set; }
        public string NomeProduto { get; set; }
        public int IdFornecedor { get; set; }
        public string NomeFornecedor { get; set; }
    }
}
