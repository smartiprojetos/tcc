﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace House_of_Taste.ClassesBDCliente
{
    class ClienteBusiness
    {
        public int Salvar(ClienteDTO dto)
        {
            ClienteDatabase db = new ClienteDatabase();
            return db.Salvar(dto);
        }

        public List<ClienteDTO> Listar()
        {
            ClienteDatabase db = new ClienteDatabase();
            return db.Listar();
        }

        public void Alterar(ClienteDTO dto)
        {
            ClienteDatabase db = new ClienteDatabase();
            db.Alterar(dto);
        }

        public void Remover(int Id)
        {
            ClienteDatabase db = new ClienteDatabase();
            db.Remover(Id);
        }

        public List<ClienteDTO> ConsultarporNome(string nome)
        {
            ClienteDatabase db = new ClienteDatabase();
            return db.ConsultarporNome(nome);
        }

        public List<ClienteDTO> ConsultarporID(int ID)
        {
            ClienteDatabase db = new ClienteDatabase();
            return db.ConsultarporID(ID);
        }
    }
}
