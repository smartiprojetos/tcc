﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace House_of_Taste.ClassesBDCliente
{
    public class ClienteDTO
    {
        public int ID { get; set; }
        public string NomeCompleto { get; set; }
        public string CPF { get; set; }
        public string Celular { get; set; }
        public int FKIDFuncionario { get; set; }
    }
}
