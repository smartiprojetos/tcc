﻿using House_of_Taste.BD;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace House_of_Taste.ClassesBDFornecedor
{
    class FornecedorDatabase
    {

        public int Salvar(FornecedorDTO dto)
        {
            string script =
            @"INSERT INTO  tb_cadastro_fornecedor
         (
            id_cadastro_fornecedor,
            nm_empresa,
            cj_cnpj,
            rn_rua,
            nr_numero,
            ct_complemento,
            uf,
            br_bairro,
            pr_unitario,
            tl_telefone,
                email,
            cd_cidade,
            cp_cep,
            fk_id_cadastro_funcionario_para_fo,

            obs_observacao
         )
         VALUES 
         (
            @id_cadastro_fornecedor,
            @nm_empresa,
            @cj_cnpj,
            @rn_rua,
            @nr_numero,
            @ct_complemento,
             @uf,
            @br_bairro,
          
            @pr_unitario,
            @tl_telefone,
            @email,
            @cd_cidade,
            @cp_cep,
            @fk_id_cadastro_funcionario_para_fo,

            @obs_observacao
         )";
            //Não ficou bem bonitinho!

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_cadastro_fornecedor", dto.Id_fornecedor));
            parms.Add(new MySqlParameter("nm_empresa", dto.Nm_empresa));
            parms.Add(new MySqlParameter("cd_cidade", dto.cidade));
            parms.Add(new MySqlParameter("uf", dto.uf));
            parms.Add(new MySqlParameter("cj_cnpj", dto.CNPJ));
            parms.Add(new MySqlParameter("rn_rua", dto.rua));
            parms.Add(new MySqlParameter("nr_numero", dto.numero));
            parms.Add(new MySqlParameter("ct_complemento", dto.complemento));
            parms.Add(new MySqlParameter("br_bairro", dto.bairro));
            parms.Add(new MySqlParameter("cp_cep", dto.cep));
            parms.Add(new MySqlParameter("tl_telefone", dto.telefone));
            parms.Add(new MySqlParameter("email", dto.email));
            parms.Add(new MySqlParameter("fk_id_cadastro_funcionario_para_fo", dto.fk_id_cadastro_funcionario_para_fo));



            parms.Add(new MySqlParameter("pr_unitario", dto.preco_unitario));
            parms.Add(new MySqlParameter("obs_observacao", dto.observacoes));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }

        public List<FornecedorDTO> Consultar(string empresa)
        {
            string script = @"Select * from tb_cadastro_fornecedor Where nm_empresa like @nm_empresa ";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_empresa", "%" + empresa + "%"));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<FornecedorDTO> lista = new List<FornecedorDTO>();
            while(reader.Read())
            {
                FornecedorDTO dto = new FornecedorDTO();
                dto.Id_fornecedor = reader.GetInt32("id_cadastro_fornecedor");
                dto.Nm_empresa = reader.GetString("nm_empresa");
                dto.CNPJ = reader.GetString("cj_cnpj");
                dto.rua = reader.GetString("rn_rua");
                dto.telefone = reader.GetString("tl_telefone");
                dto.email= reader.GetString("email");
                dto.fk_id_cadastro_funcionario_para_fo = reader.GetInt32("fk_id_cadastro_funcionario_para_fo"); 

                dto.cidade = reader.GetString("cd_cidade");
                dto.numero = reader.GetString("nr_numero");
                dto.cep = reader.GetString("cp_cep");
                dto.complemento = reader.GetString("ct_complemento");
                dto.uf = reader.GetString("uf");
                dto.bairro = reader.GetString("br_bairro");
                dto.preco_unitario = reader.GetDecimal("pr_unitario");
                dto.observacoes = reader.GetString("pr_unitario");

                lista.Add(dto);
            }
            reader.Close();
            return lista;

        }



        public List<FornecedorDTO> Listar()
        {
            string script = @"Select * from tb_cadastro_fornecedor";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<FornecedorDTO> lista = new List<FornecedorDTO>();

            while (reader.Read())
            {
                FornecedorDTO dto = new FornecedorDTO();
                dto.Id_fornecedor = reader.GetInt32("id_cadastro_fornecedor");
                dto.Nm_empresa = reader.GetString("nm_empresa");
                dto.CNPJ = reader.GetString("cj_cnpj");
                dto.rua = reader.GetString("rn_rua");
                dto.numero = reader.GetString("nr_numero");
                dto.complemento = reader.GetString("ct_complemento");
                dto.bairro = reader.GetString("br_bairro");
                dto.cidade = reader.GetString("cd_cidade");
                dto.uf = reader.GetString("uf");
                dto.telefone= reader.GetString("tl_telefone");
                dto.email = reader.GetString("email");
                dto.fk_id_cadastro_funcionario_para_fo = reader.GetInt32("fk_id_cadastro_funcionario_para_fo");


                dto.cep = reader.GetString("cp_cep");
                dto.preco_unitario = reader.GetDecimal("pr_unitario");
                dto.observacoes = reader.GetString("pr_unitario");

                lista.Add(dto);
            }
            reader.Close();
            return lista;
        }

        public void Alterar(FornecedorDTO dto)
        {
            string scritp = @"Update tb_cadastro_fornecedor Set 
                                id_cadastro_fornecedor,
                         nm_empresa = @nm_empresa,
            cj_cnpj = @cj_cnpj,
            rn_rua = @rn_rua,
            nr_numero = @nr_numero,
            ct_complemento = @ct_complemento,
            br_bairro = @br_bairro,
            pr_unitario = @pr_unitario,
            cd_cidade=@cd_cidade,
            uf=@uf,
            fk_id_cadastro_funcionario_para_fo=@fk_id_cadastro_funcionario_para_fo,


            tl_telefone = @tl_telefone,
            email=@email
            cp_cep=@cp_cep
            obs_observacao = @obs_observacao";
            

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_cadastro_fornecedor", dto.Id_fornecedor));
            parms.Add(new MySqlParameter("nm_empresa", dto.Nm_empresa));
            parms.Add(new MySqlParameter("cj_cnpj", dto.CNPJ));
            parms.Add(new MySqlParameter("cd_cidade", dto.cidade));
            parms.Add(new MySqlParameter("uf", dto.uf));
            parms.Add(new MySqlParameter("rn_rua", dto.rua));
            parms.Add(new MySqlParameter("nr_numero", dto.numero));
            parms.Add(new MySqlParameter("ct_complemento", dto.complemento));
            parms.Add(new MySqlParameter("br_bairro", dto.bairro));
            parms.Add(new MySqlParameter("cp_cep", dto.cep));
            parms.Add(new MySqlParameter("pr_unitario", dto.preco_unitario));
            parms.Add(new MySqlParameter("tl_telefone", dto.telefone));
            parms.Add(new MySqlParameter("email", dto.email));
            parms.Add(new MySqlParameter("fk_id_cadastro_funcionario_para_fo", dto.fk_id_cadastro_funcionario_para_fo));



            parms.Add(new MySqlParameter("obs_observacao", dto.observacoes));
            Database db = new Database();
           db.ExecuteInsertScript(scritp, parms);
            }


        public void Remover(int id)
        {
            string script = @"Delete from tb_cadastro_fornecedor  Where id_cadastro_fornecedor = @id_cadastro_fornecedor ";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_cadastro_fornecedor", id));
            Database db = new Database();
            db.ExecuteInsertScript(script,parms);
        }

        
    }
}
