﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace House_of_Taste.ClassesBDFornecedor
{
    class FornecedorBusiness
    {
        FornecedorDatabase db = new FornecedorDatabase();
        public int Salvar(FornecedorDTO dto)
        {          
            return db.Salvar(dto);
        }

        public List<FornecedorDTO> Listar() {
            return db.Listar();
        }

        public List<FornecedorDTO> Consultar(string empresa)
        {      
            return db.Consultar(empresa);
        }

        public void Alerar(FornecedorDTO dto)
        {
            db.Alterar(dto);
        }

        public void Remover(int id)
        {
            db.Remover(id);
        }
    }
}
