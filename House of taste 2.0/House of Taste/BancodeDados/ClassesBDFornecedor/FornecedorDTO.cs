﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace House_of_Taste.ClassesBDFornecedor
{
    public class FornecedorDTO
    {
        public int Id_fornecedor { get; set; }
        public string Nm_empresa { get; set; }
        public string CNPJ { get; set; }
        public string rua { get; set; }
        public string numero { get; set; }
        public string complemento { get; set; }
        public string bairro { get; set; }
        public string cidade { get; set; }
        public string uf { get; set; }
        public string cep { get; set; }
        public string telefone { get; set; }
        public string email { get; set; }
        public decimal preco_unitario { get; set; }
        public string observacoes { get; set; }
        public int fk_id_cadastro_funcionario_para_fo { get; set; }

    }
}
