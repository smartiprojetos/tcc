﻿using House_of_Taste.BD;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace House_of_Taste.ClassesBDCardapio
{
    class CardapioDatabase
    {
        public int Salvar(CardapioDTO dto)
        {
            string script =
                @"insert into tb_cardapio(nm_produto, vl_valor, tp_tipo, fk_id_funcionario_cp)
                values(@nm_produto,@vl_valor,@tp_tipo, @fk_id_funcionario_cp) ";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_produto", dto.Nm_produto));
            parms.Add(new MySqlParameter("vl_valor", dto.Vl_valor));
            parms.Add(new MySqlParameter("tp_tipo", dto.tp_tipo));
            parms.Add(new MySqlParameter("fk_id_funcionario_cp", dto.fk_funcinario));
            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }
        public List<CardapioDTO> Listar()
        {
            string script =
                @"Select * from tb_cardapio";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);
            List<CardapioDTO> lista = new List<CardapioDTO>();

            while (reader.Read())
            {
                CardapioDTO dto = new CardapioDTO();
                dto.Id_cardapio = reader.GetInt32("id_cardapio");
                dto.Nm_produto = reader.GetString("nm_produto");
                dto.Vl_valor = reader.GetDecimal("vl_valor");
                dto.tp_tipo = reader.GetString("tp_tipo");
                dto.fk_funcinario = reader.GetInt32("fk_id_funcionario_cp");
                lista.Add(dto);
            }
            reader.Close();
            return lista;


        }

        public void Alterar(CardapioDTO dto)
        {
            string script = @"update tb_cardapio set 
                                                         nm_produto=@nm_produto,
                                                         vl_valor=@vl_valor,
                                                         tp_tipo=@tp_tipo,
                                                         fk_id_funcionario_cp=@fk_id_funcionario_cp
                                               where        id_cardapio=@id_cardapio";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_cardapio", dto.Id_cardapio));
            parms.Add(new MySqlParameter("nm_produto", dto.Nm_produto));
            parms.Add(new MySqlParameter("vl_valor", dto.Vl_valor));
            parms.Add(new MySqlParameter("tp_tipo", dto.tp_tipo));
            parms.Add(new MySqlParameter("fk_id_funcionario_cp", dto.fk_funcinario));
            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }


        public void Remover(int id)
        {
            string script = @"delete from tb_cardapio where id_cardapio=@id_cardapio";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_cardapio", id));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }


        public List<CardapioDTO> Consultar(string Produto)
        {
            string script = @"select *from tb_cardapio where nm_produto like @nm_produto";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_produto", Produto + "%"));
            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);
            List<CardapioDTO> lista = new List<CardapioDTO>();
            CardapioDTO dto = null;
            while (reader.Read())
            {
                dto = new CardapioDTO();
                dto.Id_cardapio = reader.GetInt32("id_cardapio");
                dto.Nm_produto = reader.GetString("nm_produto");
                dto.tp_tipo = reader.GetString("tp_tipo");
                dto.Vl_valor = reader.GetDecimal("vl_valor");
                dto.fk_funcinario = reader.GetInt32("fk_id_funcionario_cp");
                lista.Add(dto);
            }
            reader.Close();
            return lista;
        }


        public List<CardapioDTO> Buscar(int id)
        {
            string script = @"select * from tb_cardapio where id_cardapio like @id_cardapio";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_cardapio", id + "%"));
            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);
            List<CardapioDTO> lista = new List<CardapioDTO>();
            CardapioDTO dto = null;
            while (reader.Read())
            {
                dto = new CardapioDTO();
                dto.Id_cardapio = reader.GetInt32("id_cardapio");
                dto.Nm_produto = reader.GetString("nm_produto");
                dto.tp_tipo = reader.GetString("tp_tipo");
                dto.Vl_valor = reader.GetDecimal("vl_valor");
                dto.fk_funcinario = reader.GetInt32("fk_id_funcionario_cp");
                lista.Add(dto);
            }
            reader.Close();
            return lista;
        }
        public List<CardapioDTO> ConsultarporNome(string nome)
        {
            string script = @"select * from tb_cardapio where nm_produto like @nm_produto";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_produto", nome + "%"));
            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);
            List<CardapioDTO> lista = new List<CardapioDTO>();
            while (reader.Read())
            {
                CardapioDTO dto = new CardapioDTO();
                dto.Id_cardapio = reader.GetInt32("id_cardapio");
                dto.Nm_produto = reader.GetString("nm_produto");
                dto.Vl_valor = reader.GetDecimal("vl_valor");
                dto.tp_tipo = reader.GetString("tp_tipo");
                dto.fk_funcinario = reader.GetInt32("fk_id_funcionario_cp");

                lista.Add(dto);
            }
            reader.Close();
            return lista;
        }

        public List<CardapioDTO> ConsultarporId(int Id)
        {
            string script = @"select * from tb_cardapio where id_cardapio like @id_cardapio";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_cardapio", Id + "%"));
            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);
            List<CardapioDTO> lista = new List<CardapioDTO>();
            while (reader.Read())
            {
                CardapioDTO dto = new CardapioDTO();
                dto.Id_cardapio = reader.GetInt32("id_cardapio");
                dto.Nm_produto = reader.GetString("nm_produto");
                dto.Vl_valor = reader.GetDecimal("vl_valor");
                dto.tp_tipo = reader.GetString("tp_tipo");
                dto.fk_funcinario = reader.GetInt32("fk_id_funcionario_cp");

                lista.Add(dto);
            }
            reader.Close();
            return lista;
        }
    }
}
