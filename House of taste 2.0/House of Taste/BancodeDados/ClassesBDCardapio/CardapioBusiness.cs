﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace House_of_Taste.ClassesBDCardapio
{
    class CardapioBusiness
    {
        public int Salvar(CardapioDTO dto)
        {
            if (dto.Nm_produto == string.Empty)
            {
                throw new ArgumentException("Nome do produto do é obrigatório, por favor informar este campo.");
            }

            if (dto.Vl_valor == 0)
            {
                throw new ArgumentException("Valor do produto é obrigatório, por favor informe-nos o valor que deseja para este produto.");
            }

            if (dto.tp_tipo == string.Empty)
            {
                throw new ArgumentException("Tipo do produto é obrigatório, por favor informe-nos.");
            } 

            CardapioDatabase db = new CardapioDatabase();
            return db.Salvar(dto);
        }

        public List<CardapioDTO> Listar()
        {
            CardapioDatabase db = new CardapioDatabase();
            return db.Listar();
        }

        public void Remover(int id)
        {
            if (id == 0)
            {
                throw new ArgumentException("Id não pode ser nulo.");
            }

            CardapioDatabase db = new CardapioDatabase();
            db.Remover(id);
        }

        public List<CardapioDTO> Consultar(string produto)
        {

            CardapioDatabase db = new CardapioDatabase();
            return db.Consultar(produto);
        }

        public void Alterar(CardapioDTO dto)
        {
            CardapioDatabase db = new CardapioDatabase();
            db.Alterar(dto);
        }

        public List<CardapioDTO> Buscar(int id)
        {
            if (id == 0)
            {
                throw new ArgumentException("Id é obrigatório.");
            }

            CardapioDatabase db = new CardapioDatabase();
            return db.Buscar(id);
        }

        public List<CardapioDTO> ConsultarporNome(string produto)
        {
            CardapioDatabase db = new CardapioDatabase();
            return db.ConsultarporNome(produto);
        }

        public List<CardapioDTO> ConsultarporId(int ID)
        {
            CardapioDatabase db = new CardapioDatabase();
            return db.ConsultarporId(ID);
        }
    }
}
