﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace House_of_Taste.ClassesBDCardapio
{
    public class CardapioDTO
    {
        public int Id_cardapio { get; set; }
        public string Nm_produto { get; set; }
        public decimal Vl_valor { get; set; }
        public string tp_tipo { get; set; }
        public int fk_funcinario { get; set; }
    }
}
