﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace House_of_Taste.BancodeDados.ClassesBDCardapio.Itemdeprodutonocardapio
{
    class ItemProdutoCardapioGrid
    {
        public string NomeProduto { get; set; }
        public string NomeProdutodoCardapio { get; set; }
        public int FkidCardapio { get; set; }
        public int FKidEstoque { get; set; }
    }
}
