﻿using House_of_Taste.BD;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace House_of_Taste.BancodeDados.ClassesBDCardapio.Itemdeprodutonocardapio
{
    class ProdutonoCardapioDataBase
    {
        public int Salvar(ProdutonoCardapioDTO dto)
        {
            string script = @"insert into tb_produto_no_cardapio (fk_id_estoque_para_pc,fk_id_cardapio_para_pc)
                                                     values     (@fk_id_estoque_para_pc,@fk_id_cardapio_para_pc)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("fk_id_estoque_para_pc", dto.FKidEstoque));
            parms.Add(new MySqlParameter("fk_id_cardapio_para_pc", dto.FkidCardapio));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }

        public void Remover(int Id)
        {
            string script = @"delete from tb_produto_no_cardapio where id_produto_no_cardapiopc like @id_produto_no_cardapiopc";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_produto_no_cardapiopc", Id));
            Database db = new Database();
            db.ExecuteInsertScript(script, parms);

        }
    }
}
