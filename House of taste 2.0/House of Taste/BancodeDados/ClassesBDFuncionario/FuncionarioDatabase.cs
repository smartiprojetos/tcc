﻿using House_of_Taste.BD;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace House_of_Taste.ClassesBDFuncionario
{
    public class FuncionarioDatabase
    {
        public int Salvar(FuncionarioDTO dto)
        {
            string script = @"INSERT into tb_cadastro_funcionario
         (nm_nome_completo,user_usuario, sn_senha,cg_cargo,nr_codigo_confirmacao,cd_recebido,cf_cpf,email,dt_data_cadastro, pr_permissao_funcionarios, pr_permissao_produto, pr_permissao_cardapio, pr_permissao_fornecedor, pr_permissao_cadastrar, pr_permissao_estoque, pr_permissao_atualizar_estoque, pr_permissao_consultar_estoque, pr_permissao_pedido_fornecedor, pr_permissao_cliente, pr_permissao_controle_cliente, pr_permissao_consultar_cliente, pr_permissao_cadastrar_funcionarios, pr_permissao_consultar_funcionarios, pr_permissao_cadastrar_novo_produto, pr_permissao_consultar_produtos, pr_permissao_consultar_cardapio, pr_permissao_cadastro_novo_cardapio, pr_permissao_novo_fornecedor, pr_permissao_consultar_fornecedor  )  
          VALUES
         (@nm_nome_completo,@user_usuario, @sn_senha,@cg_cargo,@nr_codigo_confirmacao,@cd_recebido,@email,@cf_cpf,@dt_data_cadastro,@pr_permissao_funcionarios, @pr_permissao_produto, @pr_permissao_cardapio, @pr_permissao_fornecedor, @pr_permissao_cadastrar, @pr_permissao_estoque, @pr_permissao_atualizar_estoque, @pr_permissao_consultar_estoque, @pr_permissao_pedido_fornecedor, @pr_permissao_cliente, @pr_permissao_controle_cliente, @pr_permissao_consultar_cliente , @pr_permissao_cadastrar_funcionarios, @pr_permissao_consultar_funcionarios, @pr_permissao_cadastrar_novo_produto, @pr_permissao_consultar_produtos, @pr_permissao_consultar_cardapio, @pr_permissao_cadastro_novo_cardapio, @pr_permissao_novo_fornecedor, @pr_permissao_consultar_fornecedor)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_nome_completo", dto.nomecompleto));
            parms.Add(new MySqlParameter("user_usuario", dto.usuario));
            parms.Add(new MySqlParameter("sn_senha", dto.senha));
            parms.Add(new MySqlParameter("cg_cargo", dto.cargo));
            parms.Add(new MySqlParameter("nr_codigo_confirmacao", dto.codigo_confirmacao));
            parms.Add(new MySqlParameter("cd_recebido", dto.codigo_recebido));
            parms.Add(new MySqlParameter("cf_cpf", dto.cpf));
            parms.Add(new MySqlParameter("email", dto.email));
            parms.Add(new MySqlParameter("dt_data_cadastro", dto.cadastro));
            parms.Add(new MySqlParameter("pr_permissao_funcionarios", dto.permissao_funcionarios));
            parms.Add(new MySqlParameter("pr_permissao_produto", dto.permissao_produtos));
            parms.Add(new MySqlParameter("pr_permissao_cardapio", dto.permissao_cardapio));
            parms.Add(new MySqlParameter("pr_permissao_fornecedor", dto.permissao_fornecedor));
            parms.Add(new MySqlParameter("pr_permissao_cadastrar", dto.permissao_cadastrar));
            parms.Add(new MySqlParameter("pr_permissao_estoque", dto.permissao_estoque));
            parms.Add(new MySqlParameter("pr_permissao_atualizar_estoque", dto.permissao_atualizar_estoque));
            parms.Add(new MySqlParameter("pr_permissao_consultar_estoque", dto.permissao_consultar_estoque));
            parms.Add(new MySqlParameter("pr_permissao_pedido_fornecedor", dto.permissao_pedido_fornecedor));
            parms.Add(new MySqlParameter("pr_permissao_cliente", dto.permissao_cliente));
            parms.Add(new MySqlParameter("pr_permissao_controle_cliente", dto.permissao_controle_cliente));
            parms.Add(new MySqlParameter("pr_permissao_consultar_cliente", dto.permissao_consultar_cliente));
            parms.Add(new MySqlParameter("pr_permissao_cadastrar_funcionarios", dto.permissao_cadastrar_funcionarios));
            parms.Add(new MySqlParameter("pr_permissao_consultar_funcionarios", dto.permissao_consultar_funcionarios));
            parms.Add(new MySqlParameter("pr_permissao_cadastrar_novo_produto", dto.permissao_cadastrar_novo_produto));
            parms.Add(new MySqlParameter("pr_permissao_consultar_produtos", dto.consultar_produtos));
            parms.Add(new MySqlParameter("pr_permissao_consultar_cardapio", dto.permissao_consultar_cardapio));
            parms.Add(new MySqlParameter("pr_permissao_cadastro_novo_cardapio", dto.permissao_cadastro_novo_cardapio));
            parms.Add(new MySqlParameter("pr_permissao_novo_fornecedor", dto.permissao_novo_fornecedor));
            parms.Add(new MySqlParameter("pr_permissao_consultar_fornecedor", dto.permissao_consultar_fornecedor));
            //atualizar caso necessário




            Database db = new Database();
            int pk = db.ExecuteInsertScriptWithPk(script, parms);
            return pk;
        }
        public bool Consultar(string usuario, string senha)
        {
            string script = @"select * from tb_cadastro_funcionario where
                              user_usuario = @user_usuario and
                              sn_senha = @sn_senha";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("user_usuario", usuario));
            parms.Add(new MySqlParameter("sn_senha", senha));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            if (reader.Read())
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        public List<FuncionarioDTO> Listar()
        {
            string script = @"select * from tb_cadastro_funcionario";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);
            List<FuncionarioDTO> lista = new List<FuncionarioDTO>();
            while (reader.Read())
            {
                FuncionarioDTO dto = new FuncionarioDTO();
                dto.Id_funcionario = reader.GetInt32("id_cadastro_funcionario");
                dto.nomecompleto = reader.GetString("nm_nome_completo");
                dto.usuario = reader.GetString("user_usuario");
                dto.senha = reader.GetString("sn_senha");
                dto.cargo = reader.GetString ("cg_cargo");
                dto.codigo_confirmacao = reader.GetString("nr_codigo_confirmacao");
                dto.codigo_recebido = reader.GetString("cd_recebido");
                dto.cpf = reader.GetString("cf_cpf");
                dto.email = reader.GetString("email");
                dto.cadastro = reader.GetDateTime("dt_data_cadastro");
                dto.permissao_funcionarios = reader.GetBoolean("pr_permissao_funcionarios");
                dto.permissao_produtos = reader.GetBoolean("pr_permissao_produto");
                dto.permissao_cardapio = reader.GetBoolean("pr_permissao_cardapio");
                dto.permissao_fornecedor = reader.GetBoolean("pr_permissao_fornecedor");
                dto.permissao_cadastrar = reader.GetBoolean("pr_permissao_cadastrar");
                dto.permissao_estoque = reader.GetBoolean("pr_permissao_estoque");
                dto.permissao_atualizar_estoque = reader.GetBoolean("pr_permissao_atualizar_estoque");
                dto.permissao_consultar_estoque = reader.GetBoolean("pr_permissao_consultar_estoque");
                dto.permissao_pedido_fornecedor = reader.GetBoolean("pr_permissao_pedido_fornecedor");
                dto.permissao_cliente = reader.GetBoolean("pr_permissao_cliente");
                dto.permissao_controle_cliente = reader.GetBoolean("pr_permissao_controle_cliente");
                dto.permissao_consultar_cliente = reader.GetBoolean("pr_permissao_consultar_cliente");
                dto.permissao_cadastrar_funcionarios = reader.GetBoolean("pr_permissao_cadastrar_funcionarios");
                dto.permissao_consultar_funcionarios = reader.GetBoolean("pr_permissao_consultar_funcionarios");
                dto.permissao_cadastrar_novo_produto = reader.GetBoolean("pr_permissao_cadastrar_novo_produto");
                dto.consultar_produtos = reader.GetBoolean("pr_permissao_consultar_produtos");
                dto.permissao_consultar_cardapio = reader.GetBoolean("pr_permissao_consultar_cardapio");
                dto.permissao_cadastro_novo_cardapio = reader.GetBoolean("pr_permissao_cadastro_novo_cardapio");
                dto.permissao_novo_fornecedor = reader.GetBoolean("pr_permissao_novo_fornecedor");
                dto.permissao_consultar_fornecedor = reader.GetBoolean("pr_permissao_consultar_fornecedor");
                lista.Add(dto);
            }
            reader.Close();
            return lista;

        }

        public FuncionarioDTO Login(string usuario, string senha)
        {
            string script = @"select * from tb_cadastro_funcionario where
                            user_usuario = @user_usuario and
                            sn_senha = @sn_senha";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("user_usuario", usuario));
            parms.Add(new MySqlParameter("sn_senha", senha));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            FuncionarioDTO funcionario = null;

            if (reader.Read())
            {
                funcionario = new FuncionarioDTO();
                funcionario.Id_funcionario = reader.GetInt32("id_cadastro_funcionario");
                funcionario.nomecompleto = reader.GetString("nm_nome_completo");
                funcionario.usuario = reader.GetString("user_usuario");
                funcionario.senha = reader.GetString("sn_senha");
                funcionario.cargo = reader.GetString("cg_cargo");
                funcionario.codigo_confirmacao = reader.GetString("nr_codigo_confirmacao");
                funcionario.codigo_recebido = reader.GetString("cd_recebido");
                funcionario.cpf = reader.GetString("cf_cpf");
                funcionario.email = reader.GetString("email");
                funcionario.cadastro = reader.GetDateTime("dt_data_cadastro");
                funcionario.permissao_funcionarios = reader.GetBoolean("pr_permissao_funcionarios");
                funcionario.permissao_produtos = reader.GetBoolean("pr_permissao_produto");
                funcionario.permissao_cardapio = reader.GetBoolean("pr_permissao_cardapio");
                funcionario.permissao_fornecedor = reader.GetBoolean("pr_permissao_fornecedor");
                funcionario.permissao_cadastrar = reader.GetBoolean("pr_permissao_cadastrar");
                funcionario.permissao_estoque = reader.GetBoolean("pr_permissao_estoque");
                funcionario.permissao_atualizar_estoque = reader.GetBoolean("pr_permissao_atualizar_estoque");
                funcionario.permissao_consultar_estoque = reader.GetBoolean("pr_permissao_consultar_estoque");
                funcionario.permissao_pedido_fornecedor = reader.GetBoolean("pr_permissao_pedido_fornecedor");
                funcionario.permissao_cliente = reader.GetBoolean("pr_permissao_cliente");
                funcionario.permissao_controle_cliente = reader.GetBoolean("pr_permissao_controle_cliente");
                funcionario.permissao_consultar_cliente = reader.GetBoolean("pr_permissao_consultar_cliente");
                funcionario.permissao_cadastrar_funcionarios = reader.GetBoolean("pr_permissao_cadastrar_funcionarios");
                funcionario.permissao_consultar_funcionarios = reader.GetBoolean("pr_permissao_consultar_funcionarios");
                funcionario.permissao_cadastrar_novo_produto = reader.GetBoolean("pr_permissao_cadastrar_novo_produto");
                funcionario.consultar_produtos = reader.GetBoolean("pr_permissao_consultar_produtos");
                funcionario.permissao_consultar_cardapio = reader.GetBoolean("pr_permissao_consultar_cardapio");
                funcionario.permissao_cadastro_novo_cardapio = reader.GetBoolean("pr_permissao_cadastro_novo_cardapio");
                funcionario.permissao_novo_fornecedor = reader.GetBoolean("pr_permissao_novo_fornecedor");
                funcionario.permissao_consultar_fornecedor = reader.GetBoolean("pr_permissao_consultar_fornecedor");

            }
            reader.Close();
            return funcionario;
        }


        public void AlterarSenha(int id, string senha)
        {
            string script = @"update tb_cadastro_funcionario set sn_senha = @sn_senha WHERE id_cadastro_funcionario = @id_cadastro_funcionario";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_cadastro_funcionario", id));
            parms.Add(new MySqlParameter("sn_senha", senha));
            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }

       
      
 


        public void Alterar(FuncionarioDTO dto)
        {
            string script = @"update tb_cadastro_funcionario set 
                                nm_nome_completo=@nm_nome_completo,
                                user_usuario=@user_usuario, 
                                sn_senha=@sn_senha,
                                cg_cargo=@cg_cargo,
                                    cf_cpf=@cf_cpf,
                                    email=@email,
                                dt_data_cadastro=@dt_data_cadastro, 
                                pr_permissao_funcionarios=@pr_permissao_funcionarios, 
                                pr_permissao_produto=@pr_permissao_produto, 
                                    pr_permissao_cardapio=@pr_permissao_cardapio, 
                                pr_permissao_fornecedor=@pr_permissao_fornecedor, 
                                pr_permissao_cadastrar=@pr_permissao_cadastrar, 
                                    pr_permissao_estoque=@pr_permissao_estoque, 
                                pr_permissao_atualizar_estoque=@pr_permissao_atualizar_estoque, 
                                pr_permissao_consultar_estoque=@pr_permissao_consultar_estoque, 
                                pr_permissao_pedido_fornecedor=@pr_permissao_pedido_fornecedor, 
                                pr_permissao_cliente=@pr_permissao_cliente, 
                                    pr_permissao_controle_cliente=@pr_permissao_controle_cliente, 
                                    pr_permissao_consultar_cliente=@pr_permissao_consultar_cliente, 
                                pr_permissao_cadastrar_funcionarios=@pr_permissao_cadastrar_funcionarios, 
                                pr_permissao_consultar_funcionarios=@pr_permissao_consultar_funcionarios, 
                                pr_permissao_cadastrar_novo_produto=@pr_permissao_cadastrar_novo_produto, 
                                pr_permissao_consultar_produtos=@pr_permissao_consultar_produtos, 
                                    pr_permissao_consultar_cardapio=@pr_permissao_consultar_cardapio, 
                                pr_permissao_cadastro_novo_cardapio=@pr_permissao_cadastro_novo_cardapio, 
                                    pr_permissao_novo_fornecedor=@pr_permissao_novo_fornecedor, 
                                pr_permissao_consultar_fornecedor=@pr_permissao_consultar_fornecedor
                                    where id_cadastro_funcionario=@id_cadastro_funcionario";

            //nr_codigo_confirmacao = @nr_codigo_confirmacao,
            //                    cd_recebido = @cd_recebido,
            // por enquanto vai ficar comentado poeque esta passando valor nulo.
                                                         
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_cadastro_funcionario", dto.Id_funcionario));
            parms.Add(new MySqlParameter("nm_nome_completo", dto.nomecompleto));
            parms.Add(new MySqlParameter("user_usuario", dto.usuario));
            parms.Add(new MySqlParameter("sn_senha", dto.senha));
            parms.Add(new MySqlParameter("cg_cargo", dto.cargo));
            //parms.Add(new MySqlParameter("nr_codigo_confirmacao", dto.codigo_confirmacao));
            //parms.Add(new MySqlParameter("cd_recebido", dto.codigo_recebido));
            parms.Add(new MySqlParameter("cf_cpf", dto.cpf));
            parms.Add(new MySqlParameter("email", dto.email));

            parms.Add(new MySqlParameter("dt_data_cadastro", dto.cadastro));
            parms.Add(new MySqlParameter("pr_permissao_funcionarios", dto.permissao_funcionarios));
            parms.Add(new MySqlParameter("pr_permissao_produto", dto.permissao_produtos));
            parms.Add(new MySqlParameter("pr_permissao_cardapio", dto.permissao_cardapio));
            parms.Add(new MySqlParameter("pr_permissao_fornecedor", dto.permissao_fornecedor));
            parms.Add(new MySqlParameter("pr_permissao_cadastrar", dto.permissao_cadastrar));
            parms.Add(new MySqlParameter("pr_permissao_estoque", dto.permissao_estoque));
            parms.Add(new MySqlParameter("pr_permissao_atualizar_estoque", dto.permissao_atualizar_estoque));
            parms.Add(new MySqlParameter("pr_permissao_consultar_estoque", dto.permissao_consultar_estoque));
            parms.Add(new MySqlParameter("pr_permissao_pedido_fornecedor", dto.permissao_pedido_fornecedor));
            parms.Add(new MySqlParameter("pr_permissao_cliente", dto.permissao_cliente));
            parms.Add(new MySqlParameter("pr_permissao_controle_cliente", dto.permissao_controle_cliente));
            parms.Add(new MySqlParameter("pr_permissao_consultar_cliente", dto.permissao_consultar_cliente));
            parms.Add(new MySqlParameter("pr_permissao_cadastrar_funcionarios", dto.permissao_cadastrar_funcionarios));
            parms.Add(new MySqlParameter("pr_permissao_consultar_funcionarios", dto.permissao_consultar_funcionarios));
            parms.Add(new MySqlParameter("pr_permissao_cadastrar_novo_produto", dto.permissao_cadastrar_novo_produto));
            parms.Add(new MySqlParameter("pr_permissao_consultar_produtos", dto.consultar_produtos));
            parms.Add(new MySqlParameter("pr_permissao_consultar_cardapio", dto.permissao_consultar_cardapio));
            parms.Add(new MySqlParameter("pr_permissao_cadastro_novo_cardapio", dto.permissao_cadastro_novo_cardapio));
            parms.Add(new MySqlParameter("pr_permissao_novo_fornecedor", dto.permissao_novo_fornecedor));
            parms.Add(new MySqlParameter("pr_permissao_consultar_fornecedor", dto.permissao_consultar_fornecedor));
            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }

        public void Remover(int id)
        {
            string script = @"delete from tb_cadastro_funcionario where id_cadastro_funcionario=@id_cadastro_funcionario";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_cadastro_funcionario", id));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }


        public List<FuncionarioDTO> Consultar(string usuario)
        {
            string script = @"select *from tb_cadastro_funcionario where user_usuario like @user_usuario";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("user_usuario", usuario + "%"));
            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);
            List<FuncionarioDTO> lista = new List<FuncionarioDTO>();
            FuncionarioDTO dto = null;
            while (reader.Read())
            {
                dto = new FuncionarioDTO();
                dto.Id_funcionario = reader.GetInt32("id_cadastro_funcionario");
                dto.nomecompleto = reader.GetString("nm_nome_completo");
                dto.usuario = reader.GetString("user_usuario");
                dto.senha = reader.GetString("sn_senha");
                dto.cargo = reader.GetString("cg_cargo");
                dto.codigo_confirmacao = reader.GetString("nr_codigo_confirmacao");
                dto.codigo_confirmacao = reader.GetString("cd_recebido");
                dto.cpf = reader.GetString("cf_cpf");
                dto.email = reader.GetString("email");
                dto.cadastro = reader.GetDateTime("dt_data_cadastro");
                dto.permissao_funcionarios = reader.GetBoolean("pr_permissao_funcionarios");
                dto.permissao_produtos = reader.GetBoolean("pr_permissao_produto");
                dto.permissao_cardapio = reader.GetBoolean("pr_permissao_cardapio");
                dto.permissao_fornecedor = reader.GetBoolean("pr_permissao_fornecedor");
                dto.permissao_cadastrar = reader.GetBoolean("pr_permissao_cadastrar");
                dto.permissao_estoque = reader.GetBoolean("pr_permissao_estoque");
                dto.permissao_atualizar_estoque = reader.GetBoolean("pr_permissao_atualizar_estoque");
                dto.permissao_consultar_estoque = reader.GetBoolean("pr_permissao_consultar_estoque");
                dto.permissao_pedido_fornecedor = reader.GetBoolean("pr_permissao_pedido_fornecedor");
                dto.permissao_cliente = reader.GetBoolean("pr_permissao_cliente");
                dto.permissao_controle_cliente = reader.GetBoolean("pr_permissao_controle_cliente");
                dto.permissao_consultar_cliente = reader.GetBoolean("pr_permissao_consultar_cliente");
                dto.permissao_cadastrar_funcionarios = reader.GetBoolean("pr_permissao_cadastrar_funcionarios");
                dto.permissao_consultar_funcionarios = reader.GetBoolean("pr_permissao_consultar_funcionarios");
                dto.permissao_cadastrar_novo_produto = reader.GetBoolean("pr_permissao_cadastrar_novo_produto");
                dto.permissao_produtos = reader.GetBoolean("pr_permissao_consultar_produtos");
                dto.permissao_consultar_cardapio = reader.GetBoolean("pr_permissao_consultar_cardapio");
                dto.permissao_cadastro_novo_cardapio = reader.GetBoolean("pr_permissao_cadastro_novo_cardapio");
                dto.permissao_novo_fornecedor = reader.GetBoolean("pr_permissao_novo_fornecedor");
                dto.permissao_consultar_fornecedor = reader.GetBoolean("pr_permissao_consultar_fornecedor");
                lista.Add(dto);
            }
            reader.Close();
            return lista;
        }

        public List<FuncionarioDTO> ConsultarporNome(string nome)
        {
            string script = @"select *from tb_cadastro_funcionario where nm_nome_completo like @nm_nome_completo";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_nome_completo", nome + "%"));
            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);
            List<FuncionarioDTO> lista = new List<FuncionarioDTO>();
            FuncionarioDTO dto = null;
            while (reader.Read())
            {
                dto = new FuncionarioDTO();
                dto.Id_funcionario = reader.GetInt32("id_cadastro_funcionario");
                dto.nomecompleto = reader.GetString("nm_nome_completo");
                dto.usuario = reader.GetString("user_usuario");
                dto.senha = reader.GetString("sn_senha");
                dto.cargo = reader.GetString("cg_cargo");
                dto.codigo_confirmacao = reader.GetString("nr_codigo_confirmacao");
                dto.codigo_confirmacao = reader.GetString("cd_recebido");
                dto.cpf = reader.GetString("cf_cpf");
                dto.email = reader.GetString("email");
                dto.cadastro = reader.GetDateTime("dt_data_cadastro");
                dto.permissao_funcionarios = reader.GetBoolean("pr_permissao_funcionarios");
                dto.permissao_produtos = reader.GetBoolean("pr_permissao_produto");
                dto.permissao_cardapio = reader.GetBoolean("pr_permissao_cardapio");
                dto.permissao_fornecedor = reader.GetBoolean("pr_permissao_fornecedor");
                dto.permissao_cadastrar = reader.GetBoolean("pr_permissao_cadastrar");
                dto.permissao_estoque = reader.GetBoolean("pr_permissao_estoque");
                dto.permissao_atualizar_estoque = reader.GetBoolean("pr_permissao_atualizar_estoque");
                dto.permissao_consultar_estoque = reader.GetBoolean("pr_permissao_consultar_estoque");
                dto.permissao_pedido_fornecedor = reader.GetBoolean("pr_permissao_pedido_fornecedor");
                dto.permissao_cliente = reader.GetBoolean("pr_permissao_cliente");
                dto.permissao_controle_cliente = reader.GetBoolean("pr_permissao_controle_cliente");
                dto.permissao_consultar_cliente = reader.GetBoolean("pr_permissao_consultar_cliente");
                dto.permissao_cadastrar_funcionarios = reader.GetBoolean("pr_permissao_cadastrar_funcionarios");
                dto.permissao_consultar_funcionarios = reader.GetBoolean("pr_permissao_consultar_funcionarios");
                dto.permissao_cadastrar_novo_produto = reader.GetBoolean("pr_permissao_cadastrar_novo_produto");
                dto.permissao_produtos = reader.GetBoolean("pr_permissao_consultar_produtos");
                dto.permissao_consultar_cardapio = reader.GetBoolean("pr_permissao_consultar_cardapio");
                dto.permissao_cadastro_novo_cardapio = reader.GetBoolean("pr_permissao_cadastro_novo_cardapio");
                dto.permissao_novo_fornecedor = reader.GetBoolean("pr_permissao_novo_fornecedor");
                dto.permissao_consultar_fornecedor = reader.GetBoolean("pr_permissao_consultar_fornecedor");
                lista.Add(dto);
            }
            reader.Close();
            return lista;
        }
    }
}
