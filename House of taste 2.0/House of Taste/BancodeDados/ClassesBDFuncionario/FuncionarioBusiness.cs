﻿using House_of_Taste.Utilitarios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace House_of_Taste.ClassesBDFuncionario
{
    public class FuncionarioBusiness
    {

        public void AlterarSenha(int id, string senha)
        {
            FuncionarioDatabase db = new FuncionarioDatabase();
            db.AlterarSenha(id, senha);

        }
        public int Salvar(FuncionarioDTO dto)
        {
            SHA256Cript crip = new SHA256Cript();
            exReg reg = new exReg();
            reg.ValidarNome(dto.nomecompleto);
            FuncionarioDatabase db = new FuncionarioDatabase();
            if (String.IsNullOrEmpty(dto.nomecompleto) || dto.nomecompleto.Trim().Length == 0)
            {
                throw new ArgumentException("O campo Nome só tem espaços.");
            }
            if (String.IsNullOrEmpty(dto.cargo) || dto.cargo.Trim().Length == 0)
            {
                throw new ArgumentException("O campo Cargo só tem espaços.");
            }
            if (String.IsNullOrEmpty(dto.usuario) || dto.usuario.Trim().Length == 0)
            {
                throw new ArgumentException("O campo Usuário só tem espaços.");
            }
            if (String.IsNullOrEmpty(dto.senha) || dto.senha.Trim().Length == 0)
            {
                throw new ArgumentException("O campo Senha só tem espaços.");
            }
          
            if (dto.usuario == string.Empty)
                throw new ArgumentException("O campo usuário está em branco, por favor preencher.");
            if (dto.senha == string.Empty)
                throw new ArgumentException("O campo senha está em branco, por favor preencher.");
           
            if (dto.cpf == string.Empty)
                throw new ArgumentException("O campo CEP está em branco, por favor preencher.");
            if (dto.senha != null)
            {
                dto.senha = crip.Criptografar(dto.senha);
            }
            int pk = db.Salvar(dto);
            return pk;
        }

        FuncionarioDatabase db = new FuncionarioDatabase();
        public bool Logar(string usuario, string senha)
        {
            bool logou = db.Consultar(usuario, senha);
            return logou;
        }

        public List<FuncionarioDTO> Listar()
        {
            FuncionarioDatabase db = new FuncionarioDatabase();
            return db.Listar();
        }

        public FuncionarioDTO Login(string usuario, string senha)
        {
            FuncionarioDatabase db = new FuncionarioDatabase();
            return db.Login(usuario, senha);
        }
        public void Remover(int id)
        {
            if (id == 0)
            {
                throw new ArgumentException("Id não pode ser nulo.");
            }

            FuncionarioDatabase db = new FuncionarioDatabase();
            db.Remover(id);
        }

        public void Alterar(FuncionarioDTO dto)
        {
            FuncionarioDatabase db = new FuncionarioDatabase();
            db.Alterar(dto);
        }

       
        public List<FuncionarioDTO> Consultar(string usuario)
        {
            FuncionarioDatabase db = new FuncionarioDatabase();
            return db.Consultar(usuario);
        }

        public List<FuncionarioDTO> ConsultarporNome(string nome)
        {
            FuncionarioDatabase db = new FuncionarioDatabase();
            return db.ConsultarporNome(nome);
        }


    }
}

