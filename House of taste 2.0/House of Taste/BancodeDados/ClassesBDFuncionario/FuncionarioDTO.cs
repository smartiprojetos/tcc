﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace House_of_Taste.ClassesBDFuncionario
{
    public class FuncionarioDTO
    {
        public int Id_funcionario { get; set; }
        public string nomecompleto { get; set; }
        public string usuario { get; set; }
        public string senha { get; set; }
        public string cargo { get; set; }
        public string email { get; set; }
        public string codigo_confirmacao { get; set; }
        public string codigo_recebido { get; set; }
        public bool permissao_para_admin { get; set; }
        public string cpf { get; set; }
        public DateTime cadastro { get; set; }
        public bool permissao_funcionarios { get; set; }
        public bool permissao_produtos { get; set; }
        public bool permissao_cardapio { get; set; }
        public bool permissao_fornecedor { get; set; }
        public bool permissao_cadastrar { get; set; }
        public bool permissao_estoque { get; set; }
        public bool permissao_atualizar_estoque { get; set; }
        public bool permissao_consultar_estoque { get; set; }
        public bool permissao_pedido_fornecedor { get; set; }
        public bool permissao_cliente { get; set; }
        public bool permissao_controle_cliente{ get; set; }
        public bool permissao_consultar_cliente { get; set; }
        public bool permissao_cadastrar_funcionarios { get; set; }
        public bool permissao_consultar_funcionarios { get; set; }
        public bool permissao_cadastrar_novo_produto { get; set; }
        public bool consultar_produtos { get; set; }
        public bool permissao_consultar_cardapio { get; set; }
        public bool permissao_cadastro_novo_cardapio { get; set; }
        public bool permissao_novo_fornecedor { get; set; }
        public bool permissao_consultar_fornecedor { get; set; }
    }
}
