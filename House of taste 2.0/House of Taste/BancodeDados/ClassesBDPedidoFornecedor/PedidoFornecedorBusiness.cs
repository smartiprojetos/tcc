﻿using House_of_Taste.BancodeDados.ClassesBDPedidoFornecedor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace House_of_Taste.ClassesBDPedidoFornecedor
{
    class PedidoFornecedorBusiness
    {
        public int Salvar(List<PedidoparaFonecedorGridDTO> dto)
        {


            int pk = 0;
            PedidoFornecedorDatabase db = new PedidoFornecedorDatabase();
            PedidoFornecedorDTO pedido = new PedidoFornecedorDTO();
            foreach (PedidoparaFonecedorGridDTO item in dto)
            {
                pedido.FkProdutoFornecedor = item.FkProdutoFornecedor;
                pedido.PrecoTotal = item.PrecoTotal;
                pedido.Quantidade = item.Quantidade;
                pedido.FKFuncionario = item.FKFuncionario;
                pedido.DataCompra = item.DataCompra;
                pk = db.Salvar(pedido);
            }

            return pk;
        }

        public List<ConsultarPedidoViewDTO> Listar()
        {
            PedidoFornecedorDatabase db = new PedidoFornecedorDatabase();
            return db.Listar();
        }

        public List<PedidoFornecedorDTO> ConsultarporNome(string nome)
        {
            PedidoFornecedorDatabase db = new PedidoFornecedorDatabase();
            return db.ConsultarporNome(nome);
        }
    }
}
