﻿using House_of_Taste.BD;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace House_of_Taste.ClassesBDPedidoFornecedor
{
    class PedidoFornecedorDatabase
    {
        public int Salvar(PedidoFornecedorDTO dto)
        {
            string script = @"insert into tb_pedido_para_fornecedor (qrt_quantidade_de_produto,pre_valor_total,dt_compra,fk_id_funcionario_para_prf,fk_id_produto_fornecedor_para_prf)
                                values                    (@qrt_quantidade_de_produto,@pre_valor_total,@dt_compra,@fk_id_funcionario_para_prf,@fk_id_produto_fornecedor_para_prf)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("qrt_quantidade_de_produto", dto.Quantidade));
            parms.Add(new MySqlParameter("pre_valor_total", dto.PrecoTotal));
            parms.Add(new MySqlParameter("fk_id_funcionario_para_prf", dto.FKFuncionario));
            parms.Add(new MySqlParameter("dt_compra", dto.DataCompra));
            parms.Add(new MySqlParameter("fk_id_produto_fornecedor_para_prf", dto.FkProdutoFornecedor));


            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);

        }

        public List<ConsultarPedidoViewDTO> Listar()
        {
            string script = @"select * from pedido_para_fornecedo";
            Database db = new Database();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);
            List<ConsultarPedidoViewDTO> lista = new List<ConsultarPedidoViewDTO>();
            while (reader.Read())
            {
                ConsultarPedidoViewDTO dto = new ConsultarPedidoViewDTO();
                dto.IdProdudtoFornecedor = reader.GetInt32("id_produto_fornecedor_pf");
                dto.NomeFornecedor = reader.GetString("nm_empresa");
                dto.NomeProduto = reader.GetString("nm_produto");
                dto.CNPJ = reader.GetString("cj_cnpj");
                dto.CEP = reader.GetString("cp_cep");
                dto.PrecoUnitario = reader.GetDecimal("pr_unitario");

                lista.Add(dto);
            }
            reader.Close();
            return lista;
        }


        public List<ConsultarPedidoViewDTO> ConsultarView(int ID)
        {
            string script = @"select * from pedido_para_fornecedo where id_cadastro_fornecedor like @id_cadastro_fornecedor";
            Database db = new Database();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_cadastro_fornecedor", ID + "%"));
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);
            List<ConsultarPedidoViewDTO> lista = new List<ConsultarPedidoViewDTO>();
            while (reader.Read())
            {
                ConsultarPedidoViewDTO dto = new ConsultarPedidoViewDTO();
                dto.IdProdudtoFornecedor = reader.GetInt32("id_produto_fornecedor_pf");
                dto.NomeFornecedor = reader.GetString("nm_empresa");
                dto.NomeProduto = reader.GetString("nm_produto");
                dto.CNPJ = reader.GetString("cj_cnpj");
                dto.CEP = reader.GetString("cp_cep");
                dto.PrecoUnitario = reader.GetDecimal("pr_unitario");
                dto.IdFornecedor = reader.GetInt32("id_cadastro_fornecedor");
                dto.IdProduto = reader.GetInt32("id_cadastro_produtos");
                lista.Add(dto);
            }
            reader.Close();
            return lista;
        }

        public List<PedidoFornecedorDTO> ConsultarporNome(string nome)
        {
            string script = @"select * from tb_pedido_para_fornecedor where nm_empresa like @nm_empresa";
            Database db = new Database();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_empresa", nome + "%"));
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);
            List<PedidoFornecedorDTO> lista = new List<PedidoFornecedorDTO>();
            while (reader.Read())
            {
                PedidoFornecedorDTO dto = new PedidoFornecedorDTO();
                dto.IDPedidoFornecedor = reader.GetInt32("id_pedido_para_fornecedor_prf");
                dto.PrecoTotal = reader.GetDecimal("pre_valor_total");
                dto.Quantidade = reader.GetInt32("qrt_quantidade_de_produto");
                dto.FkProdutoFornecedor = reader.GetInt32("fk_id_produto_fornecedor_para_prf");

                lista.Add(dto);
            }
            reader.Close();
            return lista;
        }
    }
}
