﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace House_of_Taste.BancodeDados.ClassesBDCorreios
{
    class CorreiosModel
    {
        public CorreiosDTO Endereco(string CEP)
        {
            string URL = $"https://viacep.com.br/ws/{CEP}/json";

            WebClient rest = new WebClient();
            rest.Encoding = Encoding.UTF8;

            string data = rest.DownloadString(URL);
            var endereco = JsonConvert.DeserializeObject<CorreiosDTO>(data);
            return endereco;
        }
    }
}
