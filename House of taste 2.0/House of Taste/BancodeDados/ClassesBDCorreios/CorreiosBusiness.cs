﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace House_of_Taste.BancodeDados.ClassesBDCorreios
{
    class CorreiosBusiness
    {
        public CorreiosDTO Endereco(string CEP)
        {
            if (CEP.Contains(" "))
                throw new ArgumentException("CEP incompleto.");

            CorreiosModel model = new CorreiosModel();
            return model.Endereco(CEP);
        }
    }
}
