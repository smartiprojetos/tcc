﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace House_of_Taste.BancodeDados.ClassesBDCorreios
{
    class CorreiosDTO
    {
        public string CEP { get; set; }
        public string Endereco { get; set; }
        public string Complemento { get; set; }
        public string UF { get; set; }
    }
}
