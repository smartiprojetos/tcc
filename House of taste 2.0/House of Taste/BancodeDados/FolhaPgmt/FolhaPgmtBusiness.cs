﻿using House_of_Taste.Utilitarios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace House_of_Taste.BancodeDados.FolhaPgmt
{
    class FolhaPgmtBusiness
    {
        public decimal SalvarFolha(FolhaPgmtDTO dto)
        {
            //ValidarNumero regexNum = new ValidarNumero();
            //regexNum.ValidarDinheiro(dto.vl_SalarioBruto.ToString());

            int dM = DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month);


            if (dto.ds_DiasTrabalhados > dM)
                throw new ArgumentException("Impossivél trabalhar essa quantidade de dias no mês decorrente!");

            FolhaPgmtDatabase db = new FolhaPgmtDatabase();
            int idFolha = db.SalvarFolha(dto);

            Folha_Pgmt calcular = new Folha_Pgmt();
            FolhaPgmtDTO folha = calcular.Folha(idFolha.ToString(), dto);
            decimal liquido = folha.vl_Liquido;
            dto.id_Folha_Pgmt = idFolha;
            db.AlterarFolha(dto);

            return liquido;
        }

        public int AlterarFolha(FolhaPgmtDTO dto)
        {
            //exReg regex = new exReg();
            //regex.ValidarSalario(dto.vl_SalarioBruto.ToString());
            FolhaPgmtDatabase db = new FolhaPgmtDatabase();
            return db.AlterarFolha(dto);
        }

        public int RemoverFolha(int idFolha)
        {
            FolhaPgmtDatabase db = new FolhaPgmtDatabase();
            return db.RemoverFolha(idFolha);
        }

        public List<view_func_folha> ConsultaPorCPF(string cpf)
        {
            FolhaPgmtDatabase db = new FolhaPgmtDatabase();
            return db.ConsultarPorCPF(cpf);
        }
    }
}
