﻿using House_of_Taste.BancodeDados.ClassesBDEstoque;
using House_of_Taste.BD;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace House_of_Taste.ClassesBDEstoque
{
    class EstoqueDatabase
    {
        public int Salvar(EstoqueDTO dto)
        {
            string script = @"insert into tb_estoque (qnt_quantidade_entrada,dt_entrada,vl_valortotal,qnt_saida,dt_saida,vl_retornovalor,fk_id_produto_para_es,fk_id_cadastro_funcionario_para_es)
                                      values (@qnt_quantidade_entrada,@dt_entrada,@vl_valortotal,@qnt_saida,@dt_saida,@vl_retornovalor,@fk_id_produto_para_es,@fk_id_cadastro_funcionario_para_es)";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("qnt_quantidade_entrada", dto.quantidade_entrada));
            parms.Add(new MySqlParameter("dt_entrada", dto.Data_entrada));
            parms.Add(new MySqlParameter("vl_valortotal", dto.valor_total));
            parms.Add(new MySqlParameter("qnt_saida", dto.quantidade_saida));
            parms.Add(new MySqlParameter("dt_saida", dto.data_saida));
            parms.Add(new MySqlParameter("vl_retornovalor", dto.retornovalor));
            parms.Add(new MySqlParameter("fk_id_produto_para_es", dto.FKIdProduto));
            parms.Add(new MySqlParameter("fk_id_cadastro_funcionario_para_es", dto.IdFuncionarioParaEs));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);

        }

        public void ALterar(EstoqueDTO dto)
        {
            string script = @"update tb_estoque set qnt_quantidade_entrada=@qnt_quantidade_entrada,
                                                    dt_entrada=@dt_entrada,
                                                    vl_valortotal=@vl_valortotal,
                                                    qnt_saida=@qnt_saida,
                                                    dt_saida=@dt_saida,
                                                    vl_retornovalor=@vl_retornovalor,
                                                    fk_id_produto_para_es=@fk_id_produto_para_es,
                                                    fk_id_cadastro_funcionario_para_es=@fk_id_cadastro_funcionario_para_es
                                                    where id_estoque =@id_estoque";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("qnt_quantidade_entrada", dto.quantidade_entrada));
            parms.Add(new MySqlParameter("dt_entrada", dto.Data_entrada));
            parms.Add(new MySqlParameter("vl_valortotal", dto.valor_total));
            parms.Add(new MySqlParameter("qnt_saida", dto.quantidade_saida));
            parms.Add(new MySqlParameter("dt_saida", dto.data_saida));
            parms.Add(new MySqlParameter("vl_retornovalor", dto.valor_total));


            parms.Add(new MySqlParameter("id_estoque", dto.Id_estoque));
            parms.Add(new MySqlParameter("fk_id_produto_para_es", dto.FKIdProduto));
            parms.Add(new MySqlParameter("fk_id_cadastro_funcionario_para_es", dto.IdFuncionarioParaEs));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);

        }

        public void Remover(int pk)
        {
            string script = @"delete from tb_estoque where id_estoque LIKE @id_estoque";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_estoque", pk));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }

        public List<EstoqueDTO> Consultarponome(string nome)
        {
            string script = @" select * from tb_estoque where nm_produto like @nm_produto";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_produto", nome + "%"));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);
            List<EstoqueDTO> lista = new List<EstoqueDTO>();
            while (reader.Read())
            {
                EstoqueDTO dto = new EstoqueDTO();
                dto.Id_estoque = reader.GetInt32("id_estoque");
                dto.quantidade_entrada = reader.GetInt32("qnt_quantidade_entrada");
                dto.Data_entrada = reader.GetDateTime("dt_entrada");
                dto.valor_total = reader.GetDecimal("vl_valortotal");
                dto.quantidade_saida = reader.GetInt32("qnt_saida");
                dto.data_saida = reader.GetDateTime("dt_saida");
                dto.retornovalor = reader.GetDecimal("vl_retornovalor");
                dto.FKIdProduto = reader.GetInt32("fk_id_produto_para_es");
                dto.IdFuncionarioParaEs = reader.GetInt32("fk_id_cadastro_funcionario_para_es");

                lista.Add(dto);
            }
            reader.Close();
            return lista;
        }

        public List<EstoqueDTO> Listar()
        {
            string script = @" select * from tb_estoque";
            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);
            List<EstoqueDTO> lista = new List<EstoqueDTO>();
            while (reader.Read())
            {
                EstoqueDTO dto = new EstoqueDTO();
                dto.Id_estoque = reader.GetInt32("id_estoque");
                dto.quantidade_entrada = reader.GetInt32("qnt_quantidade_entrada");
                dto.Data_entrada = reader.GetDateTime("dt_entrada");
                dto.valor_total = reader.GetDecimal("vl_valortotal");
                dto.quantidade_saida = reader.GetInt32("qnt_saida");
                dto.data_saida = reader.GetDateTime("dt_saida");
                dto.retornovalor = reader.GetDecimal("vl_retornovalor");
                dto.FKIdProduto = reader.GetInt32("fk_id_produto_para_es");
                dto.IdFuncionarioParaEs = reader.GetInt32("fk_id_cadastro_funcionario_para_es");

                lista.Add(dto);
            }
            reader.Close();
            return lista;
        }

        public List<EstoqueDTO> ConsultarporID(int ID)
        {
            string script = @" select * from tb_estoque where id_estoque like @id_estoque";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_estoque", ID + "%"));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);
            List<EstoqueDTO> lista = new List<EstoqueDTO>();
            while (reader.Read())
            {
                EstoqueDTO dto = new EstoqueDTO();
                dto.Id_estoque = reader.GetInt32("id_estoque");
                dto.quantidade_entrada = reader.GetInt32("qnt_quantidade_entrada");
                dto.Data_entrada = reader.GetDateTime("dt_entrada");
                dto.valor_total = reader.GetDecimal("vl_valortotal");
                dto.quantidade_saida = reader.GetInt32("qnt_saida");
                dto.data_saida = reader.GetDateTime("dt_saida");
                dto.retornovalor = reader.GetDecimal("vl_retornovalor");
                dto.FKIdProduto = reader.GetInt32("fk_id_produto_para_es");
                dto.IdFuncionarioParaEs = reader.GetInt32("fk_id_cadastro_funcionario_para_es");

                lista.Add(dto);
            }
            reader.Close();
            return lista;
        }

        public List<ViewConsultarEstoqueDTO> ConsultarEstoqueView(string produto)
        {
            string script = @" select * from consultar_estoque where nm_produto like @nm_produto";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_produto", produto + "%"));
            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);
            List<ViewConsultarEstoqueDTO> lista = new List<ViewConsultarEstoqueDTO>();
            while (reader.Read())
            {
                ViewConsultarEstoqueDTO dto = new ViewConsultarEstoqueDTO();
                dto.IDestoque = reader.GetInt32("id_estoque");
                dto.Quantidadeentrada = reader.GetInt32("qnt_quantidade_entrada");
                dto.DataEntrada = reader.GetDateTime("dt_entrada");
                dto.ValorTotal = reader.GetDecimal("vl_valortotal");
                dto.QuantidadeSaida = reader.GetInt32("qnt_saida");
                dto.DataSaida = reader.GetDateTime("dt_saida");
                dto.RetornoValor = reader.GetDecimal("vl_retornovalor");
                dto.NomeProduto = reader.GetString("nm_produto");
                dto.IDproduto = reader.GetInt32("id_cadastro_produtos");

                lista.Add(dto);
            }
            reader.Close();
            return lista;
        }

        public List<ViewConsultarEstoqueDTO> ListarEstoqueView()
        {
            string script = @" select * from consultar_estoque";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);
            List<ViewConsultarEstoqueDTO> lista = new List<ViewConsultarEstoqueDTO>();
            while (reader.Read())
            {
                ViewConsultarEstoqueDTO dto = new ViewConsultarEstoqueDTO();
                dto.IDestoque = reader.GetInt32("id_estoque");
                dto.Quantidadeentrada = reader.GetInt32("qnt_quantidade_entrada");
                dto.DataEntrada = reader.GetDateTime("dt_entrada");
                dto.ValorTotal = reader.GetDecimal("vl_valortotal");
                dto.QuantidadeSaida = reader.GetInt32("qnt_saida");
                dto.DataSaida = reader.GetDateTime("dt_saida");
                dto.RetornoValor = reader.GetDecimal("vl_retornovalor");
                dto.NomeProduto = reader.GetString("nm_produto");
                dto.IDproduto = reader.GetInt32("id_cadastro_produtos");

                lista.Add(dto);
            }
            reader.Close();
            return lista;
        }
    }
}
