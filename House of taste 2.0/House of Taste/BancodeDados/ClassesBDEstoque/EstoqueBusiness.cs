﻿using House_of_Taste.BancodeDados.ClassesBDEstoque;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace House_of_Taste.ClassesBDEstoque
{
    class EstoqueBusiness
    {
        public int Salvar(EstoqueDTO dto)
        {
            EstoqueDatabase db = new EstoqueDatabase();
            return db.Salvar(dto);
        }

        public void Remover(int id)
        {
            EstoqueDatabase db = new EstoqueDatabase();
            db.Remover(id);
        }

        public void Alterar(EstoqueDTO dto)
        {
            EstoqueDatabase db = new EstoqueDatabase();
            db.ALterar(dto);
        }

        public List<EstoqueDTO> Listar()
        {
            EstoqueDatabase db = new EstoqueDatabase();
            return db.Listar();
        }

        public List<EstoqueDTO> ConsultarporNome(string nome)
        {
            EstoqueDatabase db = new EstoqueDatabase();
            return db.Consultarponome(nome);
        }

        public List<EstoqueDTO> Consultarporid(int id)
        {
            EstoqueDatabase db = new EstoqueDatabase();
            return db.ConsultarporID(id);
        }

        public List<ViewConsultarEstoqueDTO> ConsultarEstoqueView(string produto)
        {
            EstoqueDatabase db = new EstoqueDatabase();
            return db.ConsultarEstoqueView(produto);
        }

        public List<ViewConsultarEstoqueDTO> ListarEstoqueView()
        {
            EstoqueDatabase db = new EstoqueDatabase();
            return db.ListarEstoqueView();
        }
        

    }
}
