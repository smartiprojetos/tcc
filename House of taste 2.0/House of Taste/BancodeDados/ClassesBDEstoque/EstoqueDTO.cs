﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace House_of_Taste.ClassesBDEstoque
{
    public class EstoqueDTO
    {
        public int Id_estoque { get; set; }
        public int quantidade_entrada { get; set; }
        public DateTime Data_entrada { get; set; }
        public decimal valor_total { get; set; }
        public int quantidade_saida { get; set; }
        public DateTime data_saida { get; set; }
        public decimal retornovalor { get; set; }
        public int FKIdProduto { get; set; }
        public int IdFuncionarioParaEs { get; set; }
    }
}
