﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace House_of_Taste.BancodeDados.ClassesBDPedido
{
    class Business_PedidoItem
    {
        public int Salvar(DTO_PedidoItem dto)
        {
            Database_PedidoItem db = new Database_PedidoItem();
            return db.Salvar(dto);
        }

        public List<ViewConsultarPedidosDTO> Consultar(string nome)
        {
            Database_PedidoItem db = new Database_PedidoItem();
            return db.Consultar(nome);
        }
    }
}
