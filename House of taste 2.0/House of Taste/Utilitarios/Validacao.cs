﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace House_of_Taste.Utilitarios
{
    class Validacao
    {


        public bool VerificarNome(string nome)
        { 
            /* [A-Z] --> Obrigatoriamente a primeira letra tem que ser maiuscula.
             * 
             * [a-zà-ú] ---> minuscula com ou sem acento 
             * 
             * {2,} --- > tem que aparecer no minimo duas vezes o trecho anterior a essa parte 
             * 
             * [a-zà-ú]{2,} ----> O trecho [a-zà-ú] tem que se repetir no minimo duas vezes, ou seja tem que se
             *                    digitar duas minusculas após a primeira maiuscula 
             *                    
             * [A-Z][a-zà-ú]{2,} ---> A primeira palavra digitada tem que ter a primeira letra maiuscula seguidada de
             *                        no minimo duas minusculas ou com ascento ou sem ascento que tem que se repetir no minimo 
             *                        duas vezes.
             *                        
             * \s? ---> pode ter um espaço ou não após o ultimo caractere que foi digitado.
             * 
             * \s?[A-Z][a-zà-ú']?{2,}? ---> é obrigatorio que tenha um segundo nome 
             * 
             * [ ?]---> está agrupando como opicional o trecho (\s?[A-Z][a-zà-ú]?{2,}?)
             * 
             * [A-Z][a-zà-ú']? ---> assim como o anterior porém é opcional e possui o ' porque alguns nomes tem 
             * 
             * [\s?[A-Z][a-zà-ú']?{2,}?] ---> é opicional e pode ou não repetir
             * 
             * [\s?[A-Z][a-zà-ú']?{2,}?]{1,} ---> pode repetir toda a parte dentro do [] uma ou mais vezes, mas por toda a parte 
             *                                    não ser obrigatoria dentro, não é obrigatorio
             */

            string ex = @"^[A-Z][a-zà-ú]{2,}\s?[A-Z][a-zà-ú']?{2,}?[\s?[A-Z][a-zà-ú']?{2,}?]{1,}$";
            Regex re = new Regex(ex);

            if (re.IsMatch(nome) == true) { return true; }
            else { return false; }

        }

        public bool Senha(string senha)
        {
            /*
            * * ?= ---> tem que ter
            * .* ---> qualquer um antes ou depois , mas tem que ter a parte que está obrigatoria([a-z])
            * (?=.*[a-z]) ---> tem que ter em algum lugar
            * 
            * [#$^+=!*()@%&] é a mesma coisa de [[:punct:]] 
            * .{8,16} ---> qualquer coisa antes , mas tem que ter de 8 á 16
            */

            string ex = @"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[[:punct:]]).{8,16}$";
            Regex re = new Regex(ex);
            if (re.IsMatch(senha) == true) { return true; }
            else { return false; }

        }

    }
}
